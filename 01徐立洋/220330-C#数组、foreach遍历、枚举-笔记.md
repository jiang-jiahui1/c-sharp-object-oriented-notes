数组
使用 new 设置大小
创建长度为5的整型数组

```c#
int[ ] array = new int[5];
```

[5]——方括号中的数字决定数组的长度

创建的同时初始化

```c#
int[ ] arr = new int[5]{0,1,2,3,4};
```

  省略长度

```c#
int[ ] arr = new int[ ]{0,1,2,3,4};  
```

省略new 

```c#
int[ ] arr = {0,1,2,3,4};    
```


{0,1,2,3,4}——大括号中的元素个数决定数组的长度





数组长度
数组名.Length

var初始化
C#1.0或2.0中必须指定变量的数据类型，3.0可以不明确指定他的数据类型，而是使用var关键字，实际数据类型有初始化表达式推算。例如

```c#
Var number=2009；
var strs=new string[ ]{“2008”，“2009”，“80”“20”}；
var ui= new UserInfo（）；
==>> int number=2009；
string [ ]strs=new string[ ]{“2008”，“2009”，“80”“20”}；
userinfo  ui= new UserInfo（）；
```

使用：
第一范围，可以在声明局部变量时使用，for语句中，foreach语句中，using语句中；
第二原则：必须包含一个初始化器；声明的变量初始化值不能为null；不能同一语句初始化多个隐式类型变量。



foreach遍历
语法：
    foreach (数据类型 元素(变量) in 集合或者数组)
    {
        //语句
    }

枚举
用于定义具有一组特定值的数据类型
枚举以 enum 关键字声明

```c#
enum  weekday
{
    Sun,
    Mon,
    Tue,
    Wed,
    Thu,
    Fri,
    Sat
}
```

遍历枚举元素

```c#
Foreach(var i in Enum.GetValues(typeof(weekday)))
{
     Console.WriteLine(i);
}
```

