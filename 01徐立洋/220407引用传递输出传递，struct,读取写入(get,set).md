//在方法中将值返回：**return,ref,out**
        //**return:返回**
        **//ref:引用传递：  改变实参的值， 使用前必须初始化**
        //**out:输出传递    返回多个值**

```c#

            // 模拟登录：返回登陆是否成功(bool)，如果登录失败，提示用户是用户名错误还是密码错误
            //admin 888888 两个返回值 一个bool，一个string

            string c;
            bool d;
            fanhuizhi(out c, out d);
            Console.WriteLine(c);



        }
        
        public static void fanhuizhi(out string c, out bool d)
        {
            Console.WriteLine("请输入用户名");
            string a = Console.ReadLine();
            Console.WriteLine("请输入密码");
            int b = Convert.ToInt32(Console.ReadLine());
            c = "";
            d = true;
            if (a != "admin")
            {
                c = "用户名不正确";
                d = false;
            }
            if (b != 888888)
            {
                c = "密码不正确";
                d = false;

            }
            if (a != "admin" && b != 888888)
            {
                c = "用户名密码都不正确";
                d = false;
                return;
            }
            if (d = true)
            {
                c = "登入成功";
            }
           
```



类非常相似的数据类型
struct关键字
语法结构

```c#
访问修饰符  struct  结构体名
{
     定义结构体成员；
}
```

Struct 是值类型
结构体不能包含无参的构造方法
每个字段在定义时，不能给初始值
构造方法中，必须对每个字段进行赋值
Struct 不能被继承



C#不能直接访问的数据
通过访问器访问
get读
set写
优点：
控制私有字段的可访问性
保护内部数据安全性合法性

```c#
 class Student
    {
         public string name { get; set; }  // 自动属性
         public string sex { get; set; }
         public int _age { get; set; }
        //int _age;
         public string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        public string Sex
        {
            get { return sex; }
            set
            {
                if(value=="男" || value == "女")
                {
                    this.sex = value;
                }
                else
                {
                    Console.WriteLine("不符合要求");
                    
                }
            }
        }
        public int Age
        {
            get { return _age; }
            set 
            {
                if(value>=18 && value <= 25)
                {
                    this._age = value;
                }
                else
                {
                    Console.WriteLine("年龄不符合");
                }
    
            }
        }
        
        
        
  static void Main(string[] args)
        {
        Student s1 = new Student();
            s1.Name = "哈哈";
            s1.Sex = "男";
            s1.Age = 20;
            Console.WriteLine("我叫{0}性别{1}今年{2}岁",s1.Name,s1.Sex,s1.Age);
        }
```

