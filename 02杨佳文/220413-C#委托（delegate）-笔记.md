## 委托（delegate）

#### 	一.无参委托

```C#
//定义一个无参委托
delegate void myDelegate();

public class Program
    {
        static void Main(string[] args)
        {
          
            Program p = new Program();
            myDelegate md;
            md = p.sayHello;
            md();
        }
    
    public void sayHello()
        {
            Console.WriteLine("软件技术1班");
        }
	}
```

#### 	二.有参委托

```C#
	//定义一个带参委托
    delegate int myDel(int a, int b);
static class Math
    {
        //求最大值
        public static int getMax(int a, int b)
        {
            return a > b ? a : b;
        }
	}

public class Program
    {
        static void Main(string[] args)
        {
               Math.getMax(5, 8);
            Console.WriteLine(Math.getMax(5, 8));
}                
```

#### 	

#### 	三.匿名方法：不占用任何内存

```C#
//定义一个带参委托
    delegate int myDel(int a, int b);
public class Program
    {
        static void Main(string[] args)
        {
            //调用Math里的getMax
            //匿名方法:不占用任何内存
            myDel md1 = delegate (int a, int b)
            {
                return a < b ? a : b;
            };


            Console.WriteLine(md1(5, 8));

        }
        
```

