```c#
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class TravelAgency
    {
        //            1、为旅行社编写一个程序，用于接受用户输入的旅游地点。
        //                接下来验证旅行社是否能满足用户对地点的请求，验证之后显示相应消息，
        //                给出该旅游套餐的费用。
        //注意：定义一个TravelAgency 类，该类两个字段，用于存储旅游地点和相应的套餐费用。
        //                现在，在接受用户输入的信息后，程序就会要求验证旅行社是否提供该特定的旅游套餐。
        //                此处，定义一个Travels 类，定义一个读 / 写属性以验证用户输入并对其进行检索。
        //                例如，如果旅行社只提供到“加利福尼亚”和“北京”的旅游服务。
        string tplace;
        decimal cost;
       public TravelAgency(string Tplace,decimal Cost)
        {
            this.tplace = Tplace;
            this.cost = Cost;
        }
        public string Tplace
        {
            get{ return tplace; }
        }

        public decimal Cost
        {
            get { return cost; }
        }
    }

}
```

