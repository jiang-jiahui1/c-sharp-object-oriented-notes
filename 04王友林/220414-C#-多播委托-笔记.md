###### 一.多播委托

  delegate void myDelegate();  //定义无参委托

delegate int myDel(int a, int b); //定义有参委托

class Person
{
    public void eat()
    {
        Console.WriteLine("我在吃饭"); 
    }
    public void sleep()
    {
        Console.WriteLine("我在睡觉");
    }

​    public int getMax(int a, int b)
​    {
​        return a > b ? a : b;
​    }
​    public int getMin(int a, int b)
​    {
​        return a < b ? a : b;
​    }

​    public void listen() //无参构造方法
​    {

​    }

}

class Program
{
    static void Main(string[] args)
    {
        Person p = new Person();

​        //myDelegate md = p.eat;
​        //md = md + p.sleep; //添加方法
​        //md -= p.sleep; //卸载方法

​        //myDelegate md = null;
​        //md += p.eat;

​        //if (md != null){
​        //    md();
​        //}
​        //else
​        //{
​        //    Console.WriteLine("NRE");
​        //}

​        //返回的是最后一个方法的值，前面的返回值就弃用。
​        myDel md1 = p.getMax;
​        md1 += p.getMin;

​        if (md1 != null)
​        {
​            int value = md1(5,8);

​            Console.WriteLine("{0}",value);
​        }
​        else
​        {
​            Console.WriteLine("NRE");
​        }

​    }
}

}