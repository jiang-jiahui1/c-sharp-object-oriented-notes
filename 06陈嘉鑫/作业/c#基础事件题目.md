c#基础事件题目

using System;

namespace Csarp事件基础
{
    internal class Program
    {
        class Teacher
        {
            public delegate void Class();
            public event Class ClassEvent;
            public void start()
            {
                Console.WriteLine("上课了");
                if (ClassEvent != null)
                {
                    ClassEvent();
                }
            }
        }
        class Student
        {
            public string _name;
            public Student(string name)
            {
                _name = name;
            }
            public void Sleep()
            {
                Console.WriteLine("{0}在睡觉",_name);
            }
            public void Study()
            {
                Console.WriteLine("{0}在学习", _name);
            }
        }
        static void Main(string[] args)
        {
            Student s1 = new Student("王玉琪一号");
            Student s2 = new Student("王玉琪二号");
            Teacher t = new Teacher();
            t.ClassEvent += new Teacher.Class(s1.Sleep);
            t.ClassEvent += new Teacher.Class(s2.Study);
            t.start();
        }
    }
}