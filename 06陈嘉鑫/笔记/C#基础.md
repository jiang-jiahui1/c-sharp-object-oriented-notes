#### C#基础

###### 1.数据类型

（1）整数型：int 

（2）小数型：double，float，decimal

（3）布尔型： bool

（4）字符型：string，char

（5）隐型变量：var 

###### 2.变量和常量

（1）变量

（2）常量：前面要加const

###### 3.表达式和运算符

（1）算数运算符

+，-，*，/，% ，++， --

（2）比较运算符

<，>，>=，<=，==，！=

（3）逻辑运算符

&&，||，！

（4）快捷运算符

+=，-=，*=，/=，%=

（5）三元运算符

max = a>b ? a:b

###### 4.分支结构

（1）if ...else...

if(条件)

{}

else if(条件)

{}

else

{}

（2）switch...case...

switch(常量)

{

case 值1：

​	语句块1；

​	break;

case 值2：

​	语句块2；

​	break;

default:

​	语句块3；

​	break;

}

###### 5.循环结构

（1）for循环

for(表达式1；表达式2；表达式3)

{

代码块

}

（2）foreach循环（可读不可写）

foreach(类型 变量名 in 集合或者数组)

{

代码块

}

###### 6.数组

（1）数组定义

数据类型[] 数组名 = new 数据类型[数组长度]；

数组名[下标] = 值；

（2）枚举

（3）类型转换

数值类型转字符串：

int num = 5; 

string a =  num.ToString();

字符串转数值类型：

int a = int.Parse(Console.ReadLine());

