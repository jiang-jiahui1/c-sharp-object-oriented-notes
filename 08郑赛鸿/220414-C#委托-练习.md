# C#委托练习

例题1：Program定义静态方法:  getValue()，用于获取数组arr{1,3,5}的最大值，定义一个委托，并将getValue方法传给委托实例md.

```C#
using System;

namespace TEST09委托练习2
{
    //例题1：Program定义静态方法:  getValue()，用于获取
    //数组arr{1,3,5}的最大值，定义一个委托，并将getValue方法
    //传给委托实例md.

    delegate int myDelegate(int[] arr);

    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 3, 5 };
            myDelegate md = getValue;
            int max = md(arr);
            Console.WriteLine("最大值为：{0}",max);
        }


        static int getValue(int[] arr)
        {
            int max = arr[0];
            foreach (var i in arr)
            {
                if (max<i)
                {
                    max = i;
                }
            }
            return max;
        }

    }
}

```







例题2：匿名方法：Square(int n)用于返回n的平方值。 再定义数组arr{1,3,5},定义一个Util类，成员方 法：transForm用于求数组内的每个元素的平方值

```C#
using System;

namespace TEST09委托练习
{
    //    例题2：匿名方法：Square(int n)
    //用于返回n的平方值。 再定义数组arr{1,3,5},
    //定义一个Util类，成员方 法：transForm用于求数组内的每
    //个元素的平方值
    delegate void myDelegate(int[] n);
    class Program
    {
        static void Main(string[] args)
        {
            int[] n = { 1, 3, 5 };

            myDelegate md = delegate (int[] n)
            {
                for (int i = 0; i < n.Length; i++)
                {
                    n[i] = n[i] * n[i];
                }
            };

            md(n);
            foreach (var i in n)
            {
                Console.WriteLine("平方值为：{0}",i);
            }
        }
    }
}

```