# C#继承_虚方法

### 什么是虚方法？

•派生类的方法和基类的方法有相同的签名和返回类型;

•积累的方法使用virtual标注;

•派生类的方法使用override标注。

例：

```C#
class MyBaseClass{
    virtual public void Intro(){
        Console.WriteLine("这是基类");
    }
}

class MyDerivedClass : MyBaseClass{
    public string name;
    override public void Intro(){
        Console.WriteLine("这是派生类");
    }
}
```

```C#
static void Main(string[] args){
    MyDerivedClass md = new MyDerivedClass();
    
    MyDerivedClass mbc = (MyBaseClass)md;
    mbs,Intro();
}

//结果为：	这是派生类
```

当使用基类引用mbc调用Intro方法时，方法调用

被传递到派生类并执行，这是因为：

• 基类的方法被标记为virtual

• 在派生类中有匹配的override方法



#### 覆写注意事项：

1.必须具有相同的可访问性

2.不能覆写static方法或非虚方法

3.方法、属性、索引器，事件都可以被声明为virtual和override



###  override与new区别

overrid是重写了基类的方法，new是隐藏、覆盖了基类的方法



### overload与override的区别

静态多态性:函数的响应是在编译时发生的。

动态多态性:函数的响应是在运行时发生的。



静态多态性：在编译时，方法和对象的连接机制被称为早期绑定，也被称为静态绑定。

C# 提供了两种技术来实现静态多态性。分别为：方法重载(overload)、运算符重载



动态多态性是通过 抽象类 和 虚方法(virtual,override) 实现的。





### 例题：

雇员系统，定义雇员基类，共同的属性，姓名、地址和出生日期(可有可无)，

子类：程序员，秘书，高层管理，清洁工，

他们有不同的工资算法，其中高级主管和程序员采用底薪加提成(6000)的方式，

高级主管和程序员的底薪分别是5000元和2000元 ，秘书和清洁工采用工资的方式，

工资分别是3000和1000，（以多态的方式处理程序。）

```C#
class Program
    {
        static void Main(string[] args)
        {
            //程序员
            Employee employee = new Programmer("十一","广东深圳","2003-08-25",2000M,6000M);
            employee.Calwage();
            Console.WriteLine("");
            //秘书
            Employee employee1 = new Secretary("年年", "福建龙岩", "2000-01-01", 3000M);
            employee1.Calwage();
            Console.WriteLine("");
            //高层管理
            Employee employee2 = new Management("小晓郑", "福建莆田", "2003-08-25", 5000M, 6000M);
            employee2.Calwage();
            Console.WriteLine("");
            //清洁工
            Employee employee3 = new Dustman("谭荣景", "福建宁德", "1988-10-12", 1000M);
            employee3.Calwage();
        }
    }

class Employee
    {

        private string name; //姓名

        private string address; //地址


        private string birthday; //出生日期


        private decimal regular; //基本工资


        public string Name { get => name; set => name = value; }
        public string Address { get => address; set => address = value; }
        public string Birthday { get => birthday; set => birthday = value; }
        public decimal Regular { get => regular; set => regular = value; }


        //构造方法 属性初始化
        public Employee(string Name,string Address,string Birthday,decimal Regular)
        {
            this.Name = Name;
            this.Address = Address;
            this.Birthday = Birthday;
            this.Regular = Regular;
        }

        public virtual void Calwage() //计算工资的虚方法
        {
            
        }
    }

    //程序员
    class Programmer : Employee 
    {
        decimal royalties; //提成
        public decimal Royalties { get => royalties; set => royalties = value; }

        //构造函数初始化
        public Programmer(string Name,string Address,string Birthday, decimal Regular, decimal roy) : base(Name,Address,Birthday,Regular)
        {
            this.Royalties = roy;
        }

        

        //输出 计算程序员的工资
        public override void Calwage() 
        {
            Console.WriteLine("程序员的工资情况：");
            Console.WriteLine("姓名：{0}", Name);
            Console.WriteLine("地址：{0}", Address);
            Console.WriteLine("出生日期：{0}", Birthday);
            Console.WriteLine("工资：{0:C}",Royalties+Regular);
        }
    }



    class Secretary : Employee
    {
        //无参 构造函数初始化
        public Secretary(string Name, string Address, string Birthday, decimal Regular) : base(Name, Address, Birthday, Regular)
        {

        }

        //输出 计算秘书的工资
        public override void Calwage()
        {
            Console.WriteLine("秘书的工资情况：");
            Console.WriteLine("姓名：{0}", Name);
            Console.WriteLine("地址：{0}", Address);
            Console.WriteLine("出生日期：{0}", Birthday);
            Console.WriteLine("工资：{0}", Regular);
        }
    }

   //高层管理
   class Management : Employee
    {
        decimal royalties; //提成
        public decimal Royalties { get => royalties; set => royalties = value; }

        public Management(string Name, string Address, string Birthday, decimal Regular, decimal roy) : base(Name, Address, Birthday, Regular)
        {
            this.Royalties = roy;
        }

        

        //输出 计算高层管理的工资
        public override void Calwage()
        {
            Console.WriteLine("高层管理的工资情况：");
            Console.WriteLine("姓名：{0}", Name);
            Console.WriteLine("地址：{0}", Address);
            Console.WriteLine("出生日期：{0}", Birthday);
            Console.WriteLine("工资：{0:C}", Royalties + Regular);
        }
    }
    //清洁工
    class Dustman : Employee
    {
        //无参 构造方法
        public Dustman(string Name,string Address,string Birthday,decimal Regular) : base(Name, Address, Birthday, Regular)
        {

        }

        //输出 计算清洁工的工资
        public override void Calwage()
        {
            Console.WriteLine("清洁工的工资情况：");
            Console.WriteLine("姓名：{0}", Name);
            Console.WriteLine("地址：{0}", Address);
            Console.WriteLine("出生日期：{0}", Birthday);
            Console.WriteLine("工资：{0}", Regular);
        }
    }
```

