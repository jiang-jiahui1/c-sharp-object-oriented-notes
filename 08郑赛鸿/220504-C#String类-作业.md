# String类作业

### 第一题：

一、统计下面一段文字中“类”字和“码”的个数。

 

与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。

 

1、使用循环遍历的方法来实现。

2、使用Replace方法来实现。

3、使用Split()方法来实现。

```C#
 class Program
    {
        static void Main(string[] args)
        {
            //第一题
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            int s = 0;
            foreach (var item in str)
            {
                if (item == '类')
                {
                    s = s + 1;
                }
            }
            int s1 = 0;
            foreach (var item in str)
            {

                if (item == '码')
                {
                    s1 = s1 + 1;
                }
            }

            Console.WriteLine("类的个数：{0},码的个数：{1}", s, s1);

            



            //第二种
            StringBuilder a1 = new StringBuilder();
            StringBuilder a2 = new StringBuilder();
            StringBuilder a3 = new StringBuilder();

            a1.Append("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");
            a2.Append("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");
            a3.Append("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");

            a1.Replace("类", "");
            a2.Replace("码", "");
            Console.WriteLine("类的个数：{0}", a3.Length - a1.Length);
            Console.WriteLine("码的个数：{0}", a3.Length - a2.Length);

            //第三种
            string str1 = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            char[] lei = { '类' };
            string[] i = str1.Split(lei, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("类的个数：{0}",i.Length-1);
            char[] ma = { '码' };
            string[] i1 = str1.Split(ma, StringSplitOptions.None);
            Console.WriteLine("码的个数：{0}",i1.Length-1);






            Console.ReadKey();
        }
    }
```

### 第二题：

二、去掉下面一段文字的所有空格，并统计空格数；

 

C# (英 文名为 CSharp) 是 微 软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征， 即封装、继承、多态，并且添加了 事件和委托，增强了 编程的灵 活性。C# 语 言是 一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过 程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语 言中的一些复杂特性，还提 供了可视化 工具，能够高效地 编写程序。C# 是运行 在.NE T平台之上的 编程 语言。

```C#
class Program
    {
        static void Main(string[] args)
        {
            string str = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            int s = 0;
            foreach (var item in str)
            {
                if (item == ' ')
                {
                    s = s + 1;
                }
            }
            Console.WriteLine("空格数为：{0}",s);

        }

    }
```

### 第三题：

```C#
class Program
    {
        static void Main(string[] args)
        {
            string name = "十一";
            int age = 18;
            string address = "广东深圳";
            string hope = "音乐";

            StringBuilder s = new StringBuilder();
            s.Append(name);
            s.Append(age);
            s.Append(address);
            s.Append(hope);


            Console.WriteLine(s);
        }

    }
```

