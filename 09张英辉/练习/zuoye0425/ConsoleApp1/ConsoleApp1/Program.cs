﻿using System;

namespace ConsoleApp1
{
    //二、
    //创建一个Shape类,此类包含一个名为color的数据成员(用于存放颜色值)和一个
    //GetColor方法(用于获取颜色值)，这个类还包含一个名为GetArea的虚方法。
    //用这个类创建名为Circle和Square的两个子类，这两个类都包含两个数据成员，
    //即radius和sideLen。这些派生类应提供GetArea方法的实现，以计算相应形状的
    //面积。
    class Shape
    {
        public string Color { get; set; }
        public double Radius { get; set; }
        public double RideLen { get; set; }
        public void getColor (){
            Console.WriteLine("颜色是{0}",Color);
        }
        public Shape(string color)
        {
            this.Color = color;
        }
        public virtual void GetArea()
        { 
        
        }
    }
    class Circle : Shape 
    {
        public double area;
        public Circle(string color,double radius):base(color)
        {
            area = radius * radius * 3.14;
        }
        public override void GetArea()
        {
            Console.WriteLine("圆形的面积:{0}",area);
        }
    }
    class Square : Shape
    {
        public double area;
        public Square(string color, double sideLen) : base(color)
        {
            area = sideLen * sideLen;
        }
        public override void GetArea()
        {
            Console.WriteLine("正方形的面积:{0}", area);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Shape a1 = new Circle("蓝色",5);
            a1.getColor();
            a1.GetArea();
            Shape a2 = new Square("紫色", 5);
            a2.getColor();
            a2.GetArea();
        }
    }
}
