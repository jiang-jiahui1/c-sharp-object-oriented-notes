﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            int r1 = 6371;
            int r2 = 696300;
            double p = 3.14;
            int a = 5;
            int b = 4;
            int c = 3;
            #region
            //1.声明4个函数，分别用来计算两个数的和、差、积、商。
            he(a, b);
            cha(a, b);
            ji(a, b);
            shang(a, b);
            #endregion
            #region
            //3.声明一个函数用来计算圆的面积，
            //  分别计算出地球和太阳的面积，
            //  并计算太阳的面积是地球的几倍。
            //    地球和太阳半径自行百度搜索。
            circle(r1, r2, p);
            #endregion
            #region
            //4.根据用户不同的选择计算不同形状的面积
            //    （三角形、正方形、长方形）（用不同方式实现：
            //    1.参数和面积在自定义函数中输入和输出 
            //    2.参数在主函数中接收  
            //    3.参数在主函数中接收并且在主函数中要计算面积和）
            cfx(a, b);
            zfx(a);
            sjx(a,c);
            #endregion
            #region
            //5.	定义一函数，用于求2个数中的较大数，
            //并将其返回，这2个数字在主函数中由用户输入
            max(a, b);
            #endregion
            #region
            //6.在主函数中从键盘接收X, Y , Z3个数，
            //  编写函数计算这3个数的立方和并返回
            //    计算结果：S = X3 + Y3 + Z3
            lfh(a, b, c);
            #endregion
            #region
            //7.定义一个函数，用于计算m的n次幂，m和n
            //  在主函数中由用户输入，调用自定义函数
            //    进行计算，并输出结果
            n(a, b);
            #endregion


        }
        static void he(int a, int b) {
            int jg = a + b;
            Console.WriteLine("和"+jg);
        }
        static void cha(int a, int b)
        {
            int jg = a - b;
            Console.WriteLine(+jg);
        }
        static void ji(int a, int b)
        {
            int jg = a * b;
            Console.WriteLine(+jg);
        }
        static void shang(int a, int b)
        {
            int jg = a / b;
            Console.WriteLine(+jg);
        }
        #region
        static void circle(int r1, int r2, double p) {
            int sum1 = (int)(r1 * p);
            int sum2 = (int)(r2 * 9);
            Console.WriteLine("地球的面积:" + sum1);
            Console.WriteLine("太阳的面积:" + sum2);
        }
        #endregion
        #region
        public static void cfx(int a, int b) {
            int sum = a * b;
            Console.WriteLine("长方形的面积是:" + sum);
        }
        public static void zfx(int a) {
            int sum = a * a;
            Console.WriteLine("正方形面积是:" + sum);
        }
        public static void sjx(int a, int c) {
            int sum = (a * c)/2;
            Console.WriteLine("三角形的面积:" + sum);
        }
        #endregion
        #region
        public static void max(int a, int b) {
            int sum = (b > a ? b : a);
            Console.WriteLine("最大值是:" + sum);
        }
        #endregion
        #region
        static void lfh(int a, int b, int c) {
            int sum = (a * a * a) + (b * b * b) + (c * c * c);
            Console.WriteLine("立方和:" + sum);
        }
        #endregion
        #region
        static void n(int a, int b) {
            int sum = 1;
            for (int i = 1;i <= b; i++){
                sum = sum * a;
            }
            Console.WriteLine("结果是:" + sum);
        }
        #endregion
    }
}
