﻿using System;

namespace ClassTest
{
    class Program
    {
        //角色名、性别、出处、地位、门派地位、爱侣、父亲、母亲
        //张无忌、男、倚天屠龙记、主角、明教教主、赵敏、张翠山、殷素素
        static void Main(string[] args)
        {
            #region
            //Role yt = new Role();
            //yt.name = "张无忌";
            //yt.gander = "男";
            //yt.from = "倚天屠龙记";
            //yt.status = "主角";
            //yt.mpdw = "明教教主";
            //yt.lover = "赵敏";
            //yt.father = "张翠山";
            //yt.mother = "殷素素";
            //yt.xx();
            //Console.ReadKey();
            #endregion
            Account je = new Account(1000);
            je.save(500);
            je.fetch(10000);
            je.checkBal();
        }
    }
}
