﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassTest
{
    //    1、创建一个控制台应用程序，该程序名为ClassTest,
    //    在该程序中创建一个以Role为类名的、包含以下成员的一个类。
    //角色名、性别、出处、地位、门派地位、爱侣、父亲、母亲：
//    2、在ClassTest程序中，在Role类中定义一个该类的构造函数，
//    实现给该类的字段赋值的功能。
//3、在ClassTest程序主函数中创建一个对象，并完成给该对象的
//  成员赋以下的值并完成输出：张无忌、男、倚天屠龙记、主角、
//        明教教主、赵敏、张翠山、殷素素




    class Role
    {
        public string name;
        public string gander;
        public string from;
        public string status;
        public string mpdw;
        public string lover;
        public string father;
        public string mother;
        public void xx() 
        {
            Console.WriteLine("我叫{0},我的性别是{1}, 我出自{2},我的地位是{3},我的门派地位是{4},我的爱侣是{5},我的父亲是{6},我的母亲是{7}",name,gander,from,status,mpdw,lover,father,mother);
        }
    }
}
