﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zuoye5
{
    //二、	创建一个Math类，里边定义两个静态的方法，一个用于求两个数的和，另一个用于求三个数的和，方法名都定义成Add()（利用方法的重载）
    class Math
    {
        public int A;
        public int B;
        public int C;
        public static void Add(int a,int b,int c) {
            Console.WriteLine("{0}{1}{2}", a, b, c);
        }
    }
}
