﻿using System;

namespace zuoye5
{
    class Program
    {
        //一、	创建一个Student类，类中包含三个公共成员变量：stuName(学生姓名)，stuAge(学生年龄)，stuSex(学生性别)，一个静态成员变量tcAge(共青团员退团年龄)，定义一个构造函数用于初始化学生姓名，学生年龄，学生性别，再定义一个无参构造函数，再定义一个静态的方法SayHello()，输出学生的所有信息，实例化一个对象Mary
        static void Main(string[] args)
        {
            Student Mary = new Student("小明",18,"男");
            Student.SayHello(Mary.stuName,Mary.stuAge,Mary.stuSex);
            Math sum = new Math();
            sum.a(4);
            Math.Add(sum.A, sum.B, sum.C);
        }    
    }
}
