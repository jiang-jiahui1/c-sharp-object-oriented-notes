﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zuoye5
{
    //一、	创建一个Student类，类中包含三个公共成员变量：stuName(学生姓名)，stuAge(学生年龄)，stuSex(学生性别)，一个静态成员变量tcAge(共青团员退团年龄)，定义一个构造函数用于初始化学生姓名，学生年龄，学生性别，再定义一个无参构造函数，再定义一个静态的方法SayHello()，输出学生的所有信息，实例化一个对象Mary
    class Student
    {

        public string stuName;
        public int stuAge;
        public string stuSex;
        public static int tcAge;
        public Student(string stuname,int stuage,string stusex) { 
            this.stuName = stuname;
            this.stuAge = stuage;
            this.stuSex = stusex;
        }
        public Student(){ 
            
        }
        public static void SayHello(string stuname,int stuage,string stusex){
            Console.WriteLine("{0}  {1}  {2}",stuname,stuage,stusex);
        }
    }
}
