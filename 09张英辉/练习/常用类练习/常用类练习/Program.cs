﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 常用类练习
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            //1、使用循环遍历的方法来实现。
            int time1 = 0;
            int time2 = 0;
            foreach (var item in str)
            {
                if (item.Equals('码')) {
                    time1 += 1;
                }
                if (item.Equals('类')) {
                    time2 += 1;
                }
            }
            Console.WriteLine("类字出现的个数是：{0}，码字出现的个数是：{1}",time1,time2);
            //2、使用Replace方法来实现。
            string str1 = str.Replace("码", "");
            string str2 = str.Replace("类", "");
            Console.WriteLine("码字出现的个数是:{0}",str.Length-str1.Length);
            Console.WriteLine("类字出现的个数是:{0}",str.Length-str1.Length);
            //3、使用Split()方法来实现。
            string[] a = str.Split('码');
            string[] b = str.Split('类');
            Console.WriteLine("码字出现的个数是:{0}", a.Length - 1);
            Console.WriteLine("码字出现的个数是:{0}", b.Length - 1);
            Console.Read();
        }
    }
}
