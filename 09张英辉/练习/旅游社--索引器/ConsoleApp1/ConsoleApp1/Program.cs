﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        //1、为旅行社编写一个程序，用于接受用户输入的旅游地点。
        //接下来验证旅行社是否能满足用户对地点的请求，验证之后
        //显示相应消息，给出该旅游套餐的费用。注意：定义一个
        //TravelAgency 类，该类两个字段，用于存储旅游地点和相
        //应的套餐费用。现在，在接受用户输入的信息后，程序就会
        //要求验证旅行社是否提供该特定的旅游套餐。此处，定义一
        //个Travels 类，定义一个读/写属性以验证用户输入并对其进
        //行检索。例如，如果旅行社只提供到“加利福尼亚”和“北京”的旅游服务。

        static void Main(string[] args)
        {
            TravelAgency t1 = new TravelAgency("北京", 4999M);
            TravelAgency t2 = new TravelAgency("上海", 3999M);
            Travels td = new Travels(2);
            td[0] = t1;
            td[1] = t2;
            Console.WriteLine("请输入要旅游的地点");
            string place = Console.ReadLine();
            int j = 0;
            for (int i = 0; i < place.Length; i++) {
                if (place == td[i].Tplace) {
                    Console.WriteLine("地点:{0}，花费:{1}", td[place].Tplace, td[place].Cost);
                    j++;
                    break;
                }
            }
            if (j == 0) {
                Console.WriteLine("没有该地");
            }
        }
    }
}
