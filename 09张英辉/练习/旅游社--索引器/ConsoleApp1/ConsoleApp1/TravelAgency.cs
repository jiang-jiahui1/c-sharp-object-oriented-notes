﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class TravelAgency
    {
        string _tplace;
        decimal _cost;

        public TravelAgency(String place, decimal cost) {
            this._tplace = place;
            this._cost = cost;
        }
        public String Tplace
        {
            get { return _tplace; }
        }
        public decimal Cost
        {
            get { return _cost; }
        }
    }
    
}
