﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Travels
    {
        TravelAgency[] travels; 
        public Travels(int length) {
            travels = new TravelAgency[length];
        }
        
        public TravelAgency this[int index]
        {
            get {
                if (index < 0 || index > travels.Length-1) {
                    return null;
                }return travels[index];

            }
            set {
                if (index < 0 || index > travels.Length - 1) {
                    Console.WriteLine("越界");
                    return;
                }
                this.travels[index] = value;
            }
        }
        public TravelAgency this[string place] 
        {
            get {
                    foreach (var t in travels)
                    {
                        if (t.Tplace == place)
                        {
                            return t;
                        }
                    }
                    return null;
                }
            }
        public bool isE(string tp) {
            for (int i = 0; i < travels.Length; i++) {
                if (travels[i].Tplace == tp) {
                    return true;
                }
            }
            return false;
        }
     }
}

