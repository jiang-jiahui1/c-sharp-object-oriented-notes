﻿using System;

namespace ConsoleApp1
{
    //有一个热水器（namespace），
    //包含一个加热器(10-95)，
    //一个报警器和一个显示屏，

    //当水温超过95时： 
    //1.报警器会开始发出语音（输出），
    //  告诉你水的温度 订阅事件
    //2.液晶屏也会改变水温显示，
    //  来提示已经烧开了 订阅事件
    class calorifier
    {
        public delegate void warm();
        public event warm Alarm;
        public void re() {
            Console.WriteLine("开始加热");
            if (Alarm != null)
            {
                Alarm(); 
            }
        }
    }
    class hot 
    {
        int _temp;
        public hot(int temp) {
            _temp = temp;
        }
        public void wa() {
            Console.WriteLine("温度:{0}度，正在升温",_temp);
        }
        public void heat() 
        {
            if (_temp > 95 && _temp < 100)
            {
                Console.WriteLine("温度已达{0}度 !!! 温度过高", _temp);
            }
            else if (_temp == 100)
            {
                Console.WriteLine("温度已达100度,水已烧开", _temp);
            }
            else {
                Console.WriteLine("温度:{0}度，正在升温", _temp);
            }
            
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            hot s1 = new hot(78);
            hot s2 = new hot(100);

            calorifier df = new calorifier();

            df.Alarm += s1.wa;
            df.Alarm += s2.heat;

            df.re();
        }
    }
}
