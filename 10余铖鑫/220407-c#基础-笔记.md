静态方法，静态成员变量 不属于任何一个实例化对象，只属于当前的类
如何调用：类名.静态方法/成员变量

```c#
Student s1 = new Student();
            Student.sayHi();
```

静态成员变量

```c#
        public static int tcAge = 2;
```

 字段
 成员方法
 属性
 构造器

get：读取    set：写入

```c#
public string Name { get; set; }  // 自动属性
```

```c#
public static void sayHi()
        {
            //Console.WriteLine("大家好，我叫{0},今年{1}岁，性别{2}，退团年龄{3}", name, age, sex, tcAge);
            Student s2 = new Student("Mary", 18, "女");
            Console.WriteLine("大家好，我叫{0},今年{1}岁，性别{2}，退团年龄{3}",s2.stuName, s2.stuAge, s2.stuSex, tcAge);

    }
```

属性

value:上下文关键字

```c#
     public string Name
        {
           get { return _name; }    //get读取
           set { this._name = value; }  //set方法 写入     
       }
```

