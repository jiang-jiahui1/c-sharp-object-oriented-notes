一、统计下面一段文字中“类”字和“码”的个数。

 

与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。

 

1、使用循环遍历的方法来实现。

2、使用Replace方法来实现。

3、使用Split()方法来实现。

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Program
    {

        static void Main(string[] args)
        {
            //string a = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            //int s = 0;
            //int x=0;    
            //foreach (var arg in a) 
            //{
            //    if (arg== '类')
            //    {
            //        s=s+1;
            //    }
            //    if (arg== '码')
            //    {
            //        x=x+1;
            //    }
            //}
            //Console.WriteLine("文中出现类的字：{0}",s);
            //Console.WriteLine("文中出现码的字：{0}", x);
            //Console.ReadLine();

            //StringBuilder b = new StringBuilder();
            //b.Append("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");
            //b.Replace("类", "");
            //int d = a.Length - b.Length;
            //Console.WriteLine("类{0}",d);
            //Console.ReadLine();

            //string[] result = a.Split('类');
            //Console.WriteLine(result.Length-1);
            //Console.ReadKey();
            
            //Console.WriteLine("请输入一个字符串：");
            //string str = Console.ReadLine();
            //string[] condition = { "," };
            //string[] result = str.Split(condition, StringSplitOptions.None);
            //Console.WriteLine("字符串中含有逗号的个数为：" + (result.Length - 1));
            //Console.ReadKey();
        }
    }
}

```

二、去掉下面一段文字的所有空格，并统计空格数；

 

C# (英 文名为 CSharp) 是 微 软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征， 即封装、继承、多态，并且添加了 事件和委托，增强了 编程的灵 活性。C# 语 言是 一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过 程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语 言中的一些复杂特性，还提 供了可视化 工具，能够高效地 编写程序。C# 是运行 在.NE T平台之上的 编程 语言。

方法1：

```c#
string a = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言";
            //int m = 0;
            //foreach (var f in a)
            //{
            //    if (f == ' ')
            //    {
            //        m = m + 1;
            //    }
            //}
            //Console.WriteLine(m);
            //Console.ReadKey();
```

方法2：

```c#

string b = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言";

            //string[] sArray = b.Split(' ');
            //Console.WriteLine(sArray.Length - 1);
            //Console.ReadKey();
```

方法3：

```c#
string b = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言";
int m = 0;
           string v= b.Replace(" ", "1");
            foreach (var item in v)
            {
                if (item == '1') 
                {
                    m = m + 1;
                }c
            }
            Console.WriteLine(m);
            Console.ReadKey();
```

三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder 类把这些信息连接起来并输出。

```c#
 //三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder 类把这些信息连接起来并输出。
            StringBuilder sb = new StringBuilder("姓名：余铖鑫",100);
            sb.Append(" 年龄：19岁 ");
            sb.Append(" 家庭住址：山沟沟 ");
            sb.Append(" 兴趣爱好：睡觉 ");
            Console.WriteLine(sb);
            Console.ReadLine();
```

