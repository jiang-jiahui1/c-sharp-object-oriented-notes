1. 用户输入邮箱，请验证其合法性。

   ```c#
   Console.WriteLine("请输入邮箱");
               string a = Console.ReadLine();
               Regex b = new Regex(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
               Console.WriteLine("邮箱格式检查结果为:{0}",b.IsMatch(a));
               Console.ReadKey();
   ```

   

2. 用户输入手机号码，请验证其合法性。

   

   ```c#
   Console.WriteLine("请输入电话号码");
               string telephonenumber = Console.ReadLine();
               Regex c = new Regex(@"^1\d{10}$");
               Console.WriteLine("电话格式检查结果为：{0}",c.IsMatch(telephonenumber));
   ```

3.  从“来自13145698125的高中同学说今晚他和另一位同学来我家吃晚餐。同事+8613879541685说她孩子生病了请我帮忙完成她的工作，还说请我打13549643259这个电话跟她领导说一声”该段话中提取电话号码。匹配规则参考：（\+86）?1\d{10}

   ```c#
    string nana = "从“来自13145698125的高中同学说今晚他和另一位同学来我家吃晚餐。同事+8613879541685说她孩子生病了请我帮忙完成她的工作，还说请我打13549643259这个电话跟她领导说一声”该段话中提取电话号码。";
               MatchCollection c = Regex.Matches(nana,@"1\d{10}");
               foreach (Match item in c)
               {
                   Console.WriteLine(item.Value);
               }
   ```

   4.使用Regex实现“123abc456ABC789”中每一个数字替换成“X”。

   ```c#
     string nana = "123abc456ABC789";
               Regex c = new Regex(@"\d");
               string d= c.Replace(nana, "x");
               Console.WriteLine(d);
               Console.ReadKey();
   ```

   