﻿using System;

namespace lianxi
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //接受用户输入的两个数，分别求这两个数的和，差，积，商，模


            // Console.WriteLine("请输入第一个数:");
            //int a= Convert.ToInt32(Console.ReadLine());
            // Console.WriteLine("请输入第二个数:");
            //int b= Convert.ToInt32(Console.ReadLine());
            // int c = a + b;
            // Console.WriteLine("两数之和为：{0}",c);
            // int d = a - b;
            // Console.WriteLine("两数之差为：{0}", d);
            // int e = a * b;
            // Console.WriteLine("两数之积为：{0}", e);
            // int f = a / b;
            // Console.WriteLine("两数之商为：{0}", f);
            // int g = a % b;
            // Console.WriteLine("两数之模为：{0}", g);


            //要求用户输入一个四位数，如果用户输入不是四位数，将提示错误信息，如果
            //是四位数，将这个四位数的个，十，百，千位的数字单独输出来，并将他们的和
            //求出来！
            //例：用户输入2045，输出结果为：个位：5，十位：4，百位：0，千位：2，和为：11


            //Console.WriteLine("请输入一个四位数：");
            //int soce =Convert.ToInt32(Console.ReadLine());
            //if(soce<1000 || soce>10000){
            //    Console.WriteLine("输入错误！");
            //}else {
            //    int a = soce / 1000;
            //    int b = (soce / 100) % 10;
            //    int c = (soce / 10) % 10;
            //    int d = soce % 10;
            //    int sum = a + b + c + d;
            //    Console.WriteLine("千位:{0},百位:{1},十位:{2},个位:{3},和为：{4}", a, b, c, d, sum);
            //      }


            //3.用A,B,C等级对学生的的成绩分类, 其中0 - 59是c, 60 – 80是B,
            //    81 – 100 是a等级,请用代码实现该功能,判断某位同学的分数等
            //    级,同学的的分数由用户输入,不会从控制台接受数据就自己假设分数


            //Console.WriteLine("请输入成绩：");
            //int soce=Convert.ToInt32(Console.ReadLine());
            //if (soce < 0 || soce > 100)
            //{
            //    Console.WriteLine("输入错误！");
            //}
            //else if (soce >= 81 && soce <= 100)
            //{
            //    Console.WriteLine("A");
            //}
            //else if (soce >= 60 && soce <= 80)
            //{
            //    Console.WriteLine("B");
            //}
            //else {
            //    Console.WriteLine("C");
            //}


            //4.判断用户输入的年份是不是闰年，接受用户输入的年份后，首先要判断数字
            //是不是正确的年份，年份是四位数，闰年的条件是符合下面二者之一：①
            //能被4整除，但不能被100整除；②能被400整除。 

            //Console.WriteLine("请输入年份：");
            //int year =Convert.ToInt32(Console.ReadLine());
            //if (year < 1000 || year > 10000)
            //{
            //    Console.WriteLine("输入错误！");
            //}
            //else {
            //    if (year % 4 == 0 && year % 100 != 0)
            //    {
            //        Console.WriteLine("是闰年");
            //    }
            //    else if (year % 400 == 0)
            //    {
            //        Console.WriteLine("是闰年");
            //    }
            //    else {
            //        Console.WriteLine("不是闰年");
            //    }
            //}


            //5.判断用户输入的字母是不是元音字母，A、E、I、O、U、a、e、i、o、u

            //Console.WriteLine("请输入一个字母");
            //string egls=Console.ReadLine();
            //switch (egls) 
            //{
            //    case "A":
            //    case "E":
            //    case "I":
            //    case "O":
            //    case "U":
            //    case "a":
            //    case "e":
            //    case "i":
            //    case "o":
            //    case "u":
            //        Console.WriteLine("是元音字母");
            //        break;
            //    default:
            //        Console.WriteLine("不是元音字母");
            //        break;
            //}


            //6.输入一个数，判断该数是不是3的倍数，并将结果输出
            Console.WriteLine("请输入一个数：");
            int num = Convert.ToInt32(Console.ReadLine());
            int sum = num / 3;
            if (num % 3 == 0)
            {
                Console.WriteLine("是三的倍数,倍数为：{0}", sum);
            }
            else
            {
                Console.WriteLine("不是三的倍数,倍数为：{0}", sum);
            }
            
        }
    }
}
