﻿using System;

namespace Text
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.从键盘接受5个整数，求出其中的最大值和最小值。
            //int[] num = new int[5];
            //for(int i =0;i<5;i++){
            //    Console.WriteLine("请输入第"+(i+1)+"个数");
            //    num[i]=Convert.ToInt32(Console.ReadLine());
            //}
            //int max = num[0];
            //int min = num[0];
            //for(int i = 0; i<num.Length;i++){
            //    if(max<num[i]){
            //        max = num[i];
            //    }
            //    if (min>num[i]) {
            //        min = num[i];
            //    }
            //}
            //Console.WriteLine("最大值为：" + max);
            //Console.WriteLine("最小值为：" + min);


            //2.顾客从超市采购了5件商品，编写一个程序，用于接受每件商品的价格，计算应付
            //的总金额。并分别打印出各个商品的价格以及应付的总金额。
            //int[] Money = new int[5];
            //for (int i =0;i<Money.Length;i++) {
            //    Console.WriteLine("请输入第" + (i + 1) + "个数");
            //    Money[i] = Convert.ToInt32(Console.ReadLine());
            //}
            //int sum = 0;
            //for (int i=0;i<Money.Length;i++) {
            //    sum += Money[i];
            //}
            //for (int i = 0; i < Money.Length; i++) {
            //    Console.WriteLine("第"+(i+1)+"个商品的价格为："+Money[i]);
            //}
            //Console.WriteLine("总金额为：" + sum);

            //3.输入数组中的值，将数组中的最小值与第一个元素交换，
            //最大值与最后一个元素交换，输出数组。
            //int[] num = new int[5];
            //for (int i=0;i<num.Length;i++) {
            //    Console.WriteLine("请输入第" + (i + 1) + "个数");
            //    num[i] = Convert.ToInt32(Console.ReadLine());
            //}
            //int max = num[0];
            //int min = num[0];
            //int temp = num[0];
            //int a = 0;
            //int b = 0;
            //int floa = 0;
            //for (int i = 0; i < num.Length; i++) {
            //    if (min > num[i])
            //    {
            //        min = num[i];
            //        b = i;
            //        floa++;
            //    }
            //    if (max < num[i])
            //    {
            //        max = num[i];
            //        a = i;
            //        floa++;
            //    }
            //}
            //num[b] = num[0]; //最小值与第一个数交换
            //num[0] = min;
            //if (a == 0) //若最大值所在位置是0，则上面被换到了min位置。
            //    a = min;
            //num[a] = num[num.Length - 1];//最大值与最后一个数交换
            //num[num.Length - 1] = max;

            //foreach (var i in num){
            //    Console.Write(i+" ");
            //}

            //4.将一个数组中的元素逆序输出，即第一个元素和最后一个元素交换，第二个数与倒数第二元素交换…..，例如：原数组为：9  2  5  7   8，逆序后的数组为：8   7   5  2  9
            //int[] num = new int[5];
            //for (int i=0;i<num.Length;i++) {
            //    Console.WriteLine("请输入第"+(i+1)+"个数");
            //    num[i]=Convert.ToInt32(Console.ReadLine());
            //}
            //int temp = num[0];
            //for (int i=0;i<num.Length/2;i++) {
            //    temp = num[i];
            //    num[i]=num[num.Length - i - 1];
            //    num[num.Length - i - 1] = temp;
            //}
            //foreach (var i in num) {
            //    Console.Write(i + " ");
            //}

            //5.输入一个5位的正整数，使用数组判断它是不是回文数（例如：12321是回文数）
            //int[] num = new int[5];
            //for (int i = 0; i < num.Length; i++)
            //{
            //    Console.WriteLine("请输入第" + (i + 1) + "个数");
            //    num[i] = Convert.ToInt32(Console.ReadLine());
            //}
            //if (num[0] == num[4] && num[1] == num[3])
            //{
            //    Console.WriteLine("yes");
            //}
            //else {
            //    Console.WriteLine("no");
            //}

            //6.编写一个程序，用于接受两个数组的值，将这两个数组中的值依次相加保存在第三个数组中。
             int[] num1 = {1,2,3,4,5 };
             int[] num2 = {5,4,3,2,1 };
            int[] temp = new int[5];
            for (int i=0;i<5;i++) {
                temp[i] = num1[i] + num2[i];
            }
            foreach (var i in temp) {
                Console.Write(i +" ");
            }
            //7.输入年月日，计算日期是今年的第几天。

        }
    }
}
