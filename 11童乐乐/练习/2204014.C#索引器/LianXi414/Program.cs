﻿using System;

namespace LianXi414
{
    class Program
    {
        //1、为旅行社编写一个程序，用于接受用户输入的旅游地点。接下来验证旅行
        //社是否能满足用户对地点的请求，验证之后显示相应消息，给出该旅游套餐的费用。
        //注意：定义一个TravelAgency 类，该类两个字段，用于存储旅游地点和相应的套
        //餐费用。现在，在接受用户输入的信息后，程序就会要求验证旅行社是否提供
        //该特定的旅游套餐。此处，定义一个Travels 类，定义一个读/写属性以验证用
        //户输入并对其进行检索。例如，如果旅行社只提供到“加利福尼亚”和“北京”
        //的旅游服务。

        static void Main(string[] args)
        {
            TravelAgency t1 = new TravelAgency("北京",4999);
            TravelAgency t2 = new TravelAgency("上海", 4998);
            Travels tra = new Travels(2);
            tra[0] = t1;
            tra[1] = t2;
            Console.WriteLine("请输入地点：");
            string dd = Console.ReadLine();
            TravelAgency ee = tra[dd];
            int j = 0;
            for (int i = 0; i < 2; i++)
            {
                if (tra[i]==ee)
                {
                    Console.WriteLine("旅游地:{0},费用{1}",ee.Place,ee.Cost);
                    j++;
                    break;
                }
            }
            if (j==0)
            {
                Console.WriteLine("未开放");
            }
           
        }
    }
}
