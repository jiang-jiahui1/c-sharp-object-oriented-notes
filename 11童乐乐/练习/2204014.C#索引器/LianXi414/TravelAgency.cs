﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LianXi414
{
    //：定义一个TravelAgency 类，该类两个字段，用于存储旅游地点和相应的套
    //餐费用。
    class TravelAgency
    {
        string _place;
        decimal _cost;

        public TravelAgency(string place, decimal cost) {
            this._place = place;
            this._cost = cost;
        }
        public string Place
        {
            get {
                return _place;
            }
        }
        public decimal Cost {
            get {
                return _cost; 
            }
        }

    }
}
