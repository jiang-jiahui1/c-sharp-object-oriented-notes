﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LianXi414
{
    //现在，在接受用户输入的信息后，程序就会要求验证旅行社是否提供
    //该特定的旅游套餐。此处，定义一个Travels 类，定义一个读/写属性以验证用
    //户输入并对其进行检索。例如，如果旅行社只提供到“加利福尼亚”和“北京”
    //的旅游服务。
    class Travels
    {
        TravelAgency[] pla;
        public Travels(int leng) {
            this.pla =new TravelAgency[leng];
        }
        public TravelAgency this[int index]
        {
            get {
                if (index<0||index>=pla.Length)
                {
                    return null;
                }
                return pla[index];
            }
            set {
                if (index < 0 || index >= pla.Length)
                {
                    Console.WriteLine("越界");
                    return;
                }
                this.pla[index] = value;
            }
        }
        public TravelAgency this[string place]
        {
            get {
                foreach (TravelAgency _place in pla) {
                    if (place==_place.Place)
                    {
                        return _place;
                    }  
                }
                return null;
            }
        }
    }
}
