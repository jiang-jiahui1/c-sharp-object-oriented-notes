﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassTest
{
    class Account
    {
        
        public decimal balance;

        public Account(decimal Bal) {
            balance = Bal;
        }

        //存钱
        public void save(decimal sBal) {
            Console.WriteLine("余额{0}元", balance);
            Console.WriteLine("存入{0}元",sBal);
            balance += sBal;
            Console.WriteLine("余额{0}元", balance);
        }
        //取钱
        public void fetch(decimal fBal) {
            Console.WriteLine("取出{0}元", fBal);
            if (balance - fBal < 0)
            {
                Console.WriteLine("超出余额！！");
            }
            else {
                balance -= fBal;
            }
            
        }
        //查看余额
        public void checkBal() {
            Console.WriteLine("余额{0}元", balance);
        }
    }
}
