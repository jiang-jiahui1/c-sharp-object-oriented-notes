﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassTest
{//作业一：
 //1、创建一个控制台应用程序，该程序名为ClassTest,在该程序中创建一个以Role
 //为类名的、包含以下成员的一个类。角色名、性别、出处、地位、门派地位
 //、爱侣、父亲、母亲：

    //2、在ClassTest程序中，在Role类中定义一个该类的构造函数，实现给该类的字段赋值的功能。

    //3、在ClassTest程序主函数中创建一个对象，并完成给该对象的成员赋以下的值
    //并完成输出：张无忌、男、倚天屠龙记、主角、明教教主、赵敏、张翠山、殷素素
    class Role
    {
        public string name;
        public string sex;
        public string from;
        public string status;
        public string sect;
        public string love;
        public string father;
        public string mother;

        //"{0},{1},{2},{3},{4},{5},{6},{7},{8}",name,sex,from,starus,sect,love,father,mother
        public Role(string Name, string Sex,string From,string 
            Status,string Sect,string Love,string Father,string Mother){
            this.name = Name;
            this.sex = Sex;
            this.from = From;
            this.status = Status;
            this.sect = Sect;
            this.love = Love;
            this.father = Father;
            this.mother = Mother;
            }
        public void pro() {
            Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8}"
            ,name,sex,from,status,sect,love,father,mother);
        }
    }
}
