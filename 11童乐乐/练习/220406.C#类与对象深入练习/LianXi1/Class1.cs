﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LianXi1
{
    class Student
    {
        #region
        //一、	创建一个Student类，类中包含三个公共成员变量：stuName(学生姓名)，stuAge(学生年龄)，stuSex(学生性别)，一个静态成员变量tcAge(共青团员退团年龄)，定义一个构造函数用于初始化学生姓名，学生年龄，学生性别，再定义一个无参构造函数，再定义一个静态的方法SayHello()，输出学生的所有信息，实例化一个对象Mary
        //    public string stuName;
        //    public int stuAge;
        //    public string stuSEX;
        //    public static int tcAge;
        //    public Student() { 
        //    }
        //    public Student(string stuname, int stuage, string stusEX)
        //    {
        //        this.stuName = stuname;
        //        this.stuAge = stuage;
        //        this.stuSEX = stusEX;
        //}
        //    public static void sayHello(string stuname, int stuage, string stusEX) {
        //        Console.WriteLine("{0}"+" "+"{1}"+" "+"{2}",stuname,stuage,stusEX);
        //        if (stuage > tcAge)
        //        {
        //            Console.WriteLine("已退团");
        //        }
        //        else {
        //            Console.WriteLine("未退团");
        //        }
        //    }
        #endregion
        #region
        //二、	创建一个Math类，里边定义两个静态的方法，一个用于求两个数的和，另一个用于求三个数的和，方法名都定义成Add()（利用方法的重载）
        //public int temp;
        // public Student(int bal) {
        //     temp = bal;
        // }
        // public void add(int a,int b) {
        //     temp=a+b;
        //     Console.WriteLine("{0}+{1}={2}",a,b,temp);
        // }
        // public void add(int a, int b,int c)
        // {
        //     temp = a + b + c;
        //     Console.WriteLine("{0}+{1}+{2}={3}", a, b,c,temp);
        // }

        #endregion
        #region
        //三、	编写一个程序，用于计算三个职员的工资。第一位职员默认的基本工资为1000元，
        //第二位职员除具有基本工资外，还具有住房津贴。接受用户输入的基本工资和住房津贴。
        //    第三位职员可能是经理也可能不是，如果是，则有奖金收入，应接受输入的奖金值。
        //    奖金应加到基本工资内（提示：创建一个Employee类，类中创建一个ComputeSalary()
        //    的方法，为每个不同类别的职员重载该方法）

        public int temp;
        public Student(int bal) {
            temp = bal;
        }
        public void ComputeSalary() {
            temp = 1000;
            Console.WriteLine("职员1的工资为{0}",temp);
        }
        public void ComputeSalary(int a,int b)
        {
            Console.WriteLine("请输入第二个职员的工资");
            a =Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入第二个职员的住房津贴");
            b = Convert.ToInt32(Console.ReadLine());
            temp += a + b;
            Console.WriteLine("职员2的工资为{0}", temp);
        }
        public void ComputeSalary(int t)
        {
            Console.WriteLine("第三个职员是否是经理");
            string e = Console.ReadLine();
            if (e == "是")
            {
                Console.WriteLine("请输入奖金");
                t = Convert.ToInt32(Console.ReadLine());
                temp += t;
                Console.WriteLine("职员3的工资为{0}", temp);
            }
            else {
                Console.WriteLine("职员3的工资为{0}", temp);
            }

            
        }
        #endregion
    }
}
