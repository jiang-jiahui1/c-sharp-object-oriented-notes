﻿using System;

namespace LianXi1
{
    class Program
    {

        
        static void Main(string[] args)
        {
            #region
            //模拟登录：返回登陆是否成功(bool)，如果登录失败，提示用户是用户名错误还是密码错误
            //admin 888888 两个返回值 一个bool，一个string
            //    bool result=true;
            //    Console.WriteLine("请输入账号");
            //    string user = Console.ReadLine();
            //    Console.WriteLine("请输入密码");
            //    string password = Console.ReadLine();
            //    if (login(user, password, out result))
            //    {
            //        Console.WriteLine(result);
            //    }
            //    else {
            //        Console.WriteLine(result);
            //    }

            //}
            //static bool login(string user, string password,out bool result)
            //{
            //    if (user == "admin" && password == "888888")
            //    {
            //        Console.WriteLine("登录成功");
            //        result = true;
            //        return true;
            //    }
            //    else if (user == "admin")
            //    {
            //        Console.WriteLine("密码错误");
            //        result = false;
            //        return false;
            //    }
            //    else {
            //        Console.WriteLine("账号错误");
            //        result = false;
            //        return false;
            //    }
            #endregion
            #region
            //一、	创建一个Student类，类中包含三个公共成员变量：stuName(学生姓名)，stuAge(学生年龄)，stuSex(学生性别)，一个静态成员变量tcAge(共青团员退团年龄)，定义一个构造函数用于初始化学生姓名，学生年龄，学生性别，再定义一个无参构造函数，再定义一个静态的方法SayHello()，输出学生的所有信息，实例化一个对象Mary
            //int tcAge = 9;
            //Student Mary = new Student("小明",18,"男");
            //Student.sayHello(Mary.stuName,Mary.stuAge,Mary.stuSEX);


            //Console.WriteLine(Mary.stuName+" "+ Mary.stuAge+" "+ Mary.stuSEX+ " "+tcAge);
            #endregion
            #region
            //二、	创建一个Math类，里边定义两个静态的方法，一个用于求两个数的和，另一个用于求三个数的和，方法名都定义成Add()（利用方法的重载）
            //int a = 10;
            //int b = 20;
            //int c = 30;
            //Student st = new Student(0);
            //st.add(a,b);
            //st.add(a,b,c);
            #endregion
            #region
            //三、	编写一个程序，用于计算三个职员的工资。第一位职员默认的基本工资为
            //1000元，第二位职员除具有基本工资外，还具有住房津贴。接受用户输入的基本工
            //    资和住房津贴。第三位职员可能是经理也可能不是，如果是，则有奖金收入，
            //    应接受输入的奖金值。奖金应加到基本工资内（提示：创建一个Employee类，
            //    类中创建一个ComputeSalary()的方法，为每个不同类别的职员重载该方法）
            int a=0;
            int b=0;
            int c=0;
            Student top1 = new Student(1000);
            Student top2 = new Student(1000);
            Student top3 = new Student(1000);
            top1.ComputeSalary();
            top2.ComputeSalary(a,b);
            top3.ComputeSalary(c);
            #endregion

        }


    }
}
