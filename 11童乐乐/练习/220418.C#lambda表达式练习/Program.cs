﻿using System;

//匿名方法--》3.0   lambda：

namespace lambda表达式
{
    delegate int myDelegate(int x);
    class Program
    {
        static void Main(string[] args)
        {
            myDelegate md = delegate (int x)
            {
                return x + 1;
            };

            //显式: Action C#中自己定义的一个委托  Func
            myDelegate md1 =  (int x) => x + 1;
            //隐式：确定了返回值类型，可以使用隐式
            myDelegate md2 = (x) => x + 1;
            //当且只有一个参数的时候才能省略小括号
            myDelegate md3 = x => x + 1;


            Console.WriteLine(md(5));
            Console.WriteLine(md1(5));
            Console.WriteLine(md2(5));
            Console.WriteLine(md3(5));
        }

        //public static int getValue(int x)
        //{
        //    return x;
        //}
    }

    
}
