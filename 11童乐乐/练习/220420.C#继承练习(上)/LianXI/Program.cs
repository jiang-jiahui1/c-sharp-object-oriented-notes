﻿using System;

namespace 图形类
{
    class Program
    {
        //      1.图形类：
        //perimeter,area
        //求周长，面积（三角形， 四边形， 圆形）
        static void Main(string[] args)
        {
            triangle tr = new triangle();
            tr.name = "三角形";
            tr.a = 5;
            tr.b = 3;
            tr.c = 4;
            tr.getPeri();
            tr.graVlue();
            quadrangle qu = new quadrangle();
            qu.longs = 3;
            qu.wides = 4;
            qu.name = "长方形";
            qu.getPeri();
            qu.graVlue();
            round ro = new round();
            ro.name = "圆形";
            ro.radius = 3;
            ro.getPeri();
            ro.graVlue();

        }
    }
}
