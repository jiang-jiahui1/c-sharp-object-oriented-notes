﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 图形类
{
    //      1.图形类：
    //perimeter,area
    //求周长，面积（三角形， 四边形， 圆形）
    class graph
    {
        //名字
        protected string _name;
        public string name
        {
            get { return _name; }
            set { this._name = value; }
        }
        //周长
        protected double _perimeter;
        //面积
        protected double _area;
        public void graVlue() {
            Console.WriteLine("类型:{0}\n周长:{1}\n面积:{2}", _name,_perimeter,_area);
        }
    }
    //三角形
    class triangle : graph {
        public double a;
        public double b;
        public double c;
        public void getPeri() {
            _perimeter = a + b + c;
            double h = _perimeter / 2;
            _area = Math.Sqrt(h*(h-a)*(h-b)*(h-c));
        }
    }
    //长方形
    class quadrangle : graph { 
        public double longs;
        public double wides;
        public void getPeri() {
            _perimeter = (longs + wides)*2;
            _area = longs * wides;
        }
    }
    //圆形
    class round : graph {
        public double radius;
        public void getPeri() {
            _perimeter = radius * 3.14 * 2;
            _area = radius * radius * 3.14;
        }
    }
}
