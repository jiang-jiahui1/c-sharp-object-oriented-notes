﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 员工类
{
//    2.员工类Employee
// 字段：姓名，工作年限，月薪、
//组长： 月薪+1000* 年限
//经理 ：月薪+1000* 年限*  字段：基础分红1000
//客户经理：月薪+1000* 年限* 分红(基础分红*3)
//求组长，经理的年薪。
    class Employee
    {
        //姓名
        protected string name;
        public string Name{
            get { return name; }
            set { name = value; }
        }
        protected int yearmoney;
        //工作年限
        protected int year;
        //月薪
        protected int monthly;
        public void salary() {
            Console.WriteLine("姓名:{0}\n年限:{1}\n月薪:{2}\n年薪{3}",name,year,monthly,yearmoney);
        }
    }
    //组长
    class group : Employee {
        //组长： 月薪+1000*年限
        public int age;
        public int salarys;
        public void money() {
            year = age;
            monthly = salarys+(1000*year);
            yearmoney = monthly * 12;
        }
    }
    //经理
    class manager : Employee
    {//经理 ：月薪+1000* 年限*  字段：基础分红1000
        public int profit;
        public int age;
        public int salarys;
        public void money() {
            year = age;
            monthly = salarys + (1000 * year) + 1000;
            yearmoney = monthly * 12;
        }
    }
    //客户经理
    class customer : Employee {
        //客户经理：月薪+1000* 年限* 分红(基础分红*3)
        public int profit;
        public int age;
        public int salarys;
        public void money()
        {
            year = age;
            monthly = salarys + (1000 * year) + (1000*3);
            yearmoney = monthly * 12;
        }
    }
}
