﻿using System;

namespace 汽车类
{
    class Program
    {
//3.设计一个汽车类Vehicle，包含的属性有车轮个数wheels和车重weight。
//小车类Car是Vehicle的子类，其中包含的属性有载人数loader。
//卡车类Truck是Car类的子类，其中包含的属性有载重量payload。
//每个类都有构造方法和输出相关数据的方法。
        static void Main(string[] args)
        {

            Car c = new Car(2,2,2);
            c.small();
            Truck tr = new Truck(3,3,3);
            tr.small();
        }
    }
}
