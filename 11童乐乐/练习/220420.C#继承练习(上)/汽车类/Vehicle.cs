﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 汽车类
{//3.设计一个汽车类Vehicle，包含的属性有车轮个数wheels和车重weight。
 //小车类Car是Vehicle的子类，其中包含的属性有载人数loader。
 //卡车类Truck是Car类的子类，其中包含的属性有载重量payload。
 //每个类都有构造方法和输出相关数据的方法。
    class Vehicle
    {
        protected string _name;
        //车轮个数
        protected int _wheels;
        //车重
        protected int _weight;
        public Vehicle(string name,int wheels,int weight) {
            this._name = name;
            this._weight = weight;
            this._wheels = wheels;
        }
        public Vehicle()
        {

        }
        public void small() {
            Console.WriteLine("车轮数：{0}车重：{1}",_wheels,_weight);
        }
    }
    class Car : Vehicle {
        protected int _payload;
        public Car(string name, int wheels, int weight, int payload)
        {
            this._name = name;
            this._wheels = wheels;
            this._weight = weight;
            this._payload = payload;
        }
        public Car(){

        }
        public void small()
        {
            Console.WriteLine("车轮数：{0}车重：{1}载重量：{2}", _wheels, _weight, _payload);
        }
    }
    class Truck:Car {
        protected int _loader;
        public Truck(int wheels, int weight, int loader)
        {
            this._wheels = wheels;
            this._weight = weight;
            this._loader = loader;
        }
        public void small()
        {
            Console.WriteLine("车轮数：{0}车重：{1}载人数：{2}", _wheels, _weight, _loader);
        }
    }
}
