﻿using System;

namespace 学生类
{
    class Program
    {
      //要求测试结果如下： 
      //  姓名：王雷 年龄：17  学位：专科 专业：java
      //姓名：刘文 年龄：22  学位：本科 研究方向：网络技术

        static void Main(string[] args)
        {
            Specialty sp = new Specialty("王雷",17,"专科","java");
            sp.ins();
            Undergraduate un = new Undergraduate("刘文", 22, "本科", "网络技术");
            un.ins();
        }
    }
}
