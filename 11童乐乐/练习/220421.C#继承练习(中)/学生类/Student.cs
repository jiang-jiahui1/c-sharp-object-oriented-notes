﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 学生类
{
    //包括的属性有姓名name，年龄age，学位degree。
    //由学生类Student
    //派生出专科生类Specialty和本科生类Undergraduate
    //专科生类包含的属性有专业spec,
    //本科生类包括的属性有研究方向drec。
    //每个类都有相关数据的输出方法。最后在一个测试类中对设计的类进行测试。 

    class Student
    {
        protected string _name { set; get; }
        protected int _age { set; get; }
        protected string _degree { set; get; }
        public Student(string name, int age, string degree) {
            this._name = name;
            this._age = age;
            this._degree = degree;
        }
    }
    class Specialty : Student {
        public string spec;
        public Specialty(string name,int age ,string degree,string spec) :base(name,age,degree) {
           this.spec = spec;
        }
        public void ins() {
            Console.WriteLine("姓名：{0}  年龄：{1}  学位：{2} 专业：{3}", _name, _age, _degree, spec);
        }
    }
    class Undergraduate:Student
    {
        public string drec;
        public Undergraduate(string name,int age, string degree, string drec) : base(name, age, degree)
        {
            this.drec = drec;
        }
        public void ins()
        {
            Console.WriteLine("姓名：{0}  年龄：{1}  学位：{2} 研究方向m, ：{3}", _name, _age, _degree, drec);
        }
    }
}
