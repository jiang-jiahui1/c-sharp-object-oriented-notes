﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 雇员系统
{//雇员系统，定义雇员基类，共同的属性，姓名、地址和出生日期(可有可无)，子类：程序员，秘书，高层管理，清洁工，他们有不同的工资算法，其中高级主管和程序员采用底薪加提成的方式，高级主管和程序员的底薪分别是5000元和2000元 ，秘书和清洁工采用工资的方式，工资分别是3000和1000，
    class Invite
    {
        protected string _name { get; set; }
        protected string _site { get; set; }
        protected string _birthday { get; set; }
        protected decimal _money { get; set; }
        protected string _post { get; set; }
        public Invite(string name ,string site ,string birthday ,decimal money,string post)
        {
            this._name = name;
            this._site = site;
            this._birthday = birthday;
            this._money = money;
            this._post = post;
        }
        public void output() {
            Console.WriteLine("姓名:{0}  职位：{1}  地址：{2}  生日：{3}  工资：{4}",_name, _post,_site, _birthday,_money);
        }
    }
    //高级主管和程序员采用底薪加提成的方式，高级主管和程序员的底薪分别是5000元和2000元
    class Prosup:Invite
    {
        public decimal Rake;
        public Prosup(string name, string site, string birthday, decimal money,string post) : base(name, site, birthday, money, post)
        {
            Rake = 0.2M;
            _money = money + (money * Rake);
        }
    }
    //秘书和清洁工采用工资的方式，工资分别是3000和1000，
    class Clesec {
    
    }
}
