﻿using System;
using System.Text;
namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            //题目一
            //StringBuilder s1 = new StringBuilder();
            //StringBuilder s2 = new StringBuilder();
            //StringBuilder s3 = new StringBuilder();
            //s1.Append("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");

            //s2.Append("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");
            //s3.Append("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");
            string st = ("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");
            int  temp1 = 0;
            int  temp2 = 0;
            foreach (var item in st)
            {
                if (item.Equals("码"))
                {
                    temp1 += 1;
                }
                if (item.Equals("类"))
                {
                    temp2 += 1;
                }
            }
            Console.WriteLine(temp1);
            Console.WriteLine(temp2);
            //string[] str1 = st.Split("码");
            //string[] str2 = st.Split("类");
            //Console.WriteLine("码的个数:{0}",str1.Length-1);
            //Console.WriteLine("类的个数:{0}",str2.Length-1);

            //s1.Replace("码","");
            //s2.Replace("类","");
            //int a= s3.Length - s1.Length;
            //int b = s3.Length - s2.Length;
            //Console.WriteLine("码的字数:{0}",a);
            //Console.WriteLine("类的字数:{0}",b);


            //题目二
            //StringBuilder str1 = new StringBuilder();
            //StringBuilder str2 = new StringBuilder();
            //str1.Append("C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。");
            //str2.Append("C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。");
            //str1.Replace(" ","");
            //int c = str2.Length - str1.Length;
            //Console.WriteLine(str1);
            //Console.WriteLine("空格的个数是{0}个",c);


            //题目三
            //三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder 类把这些信息连接起来并输出。
            StringBuilder str = new StringBuilder();
            //Console.WriteLine("请输入你的姓名：");
            //string name = Console.ReadLine();
            //Console.WriteLine("请输入你的年龄：");
            //int age = Convert.ToInt16(Console.ReadLine());
            //Console.WriteLine("请输入你的家庭住址：");
            //string from = Console.ReadLine();
            //Console.WriteLine("请输入你的兴趣爱好：");
            //string love = Console.ReadLine();
            //Console.WriteLine(str.Append("我是"+name+"我今年"+age+"岁"+"家住"+from+"兴趣爱好是"+love));
           
        }
    }

}
