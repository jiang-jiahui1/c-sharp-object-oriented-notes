﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace 集合
{
    //   作业1：输入班级人数，输入每个人的学号，姓名、语，数，英成绩进入集合
    //   求语文的总分，数学的平均分，英语的最高分的人的姓名
    //   （使用ArrayList）
    //数据：学号姓名语文数学英语
    //   1	乔峰	85	 42	  67
    //   2	段誉	94	 34	  46
    //   3	虚竹	99	 99	  99

    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            Student stu1 = new Student(1, "乔峰", 85, 42, 67);
            Student stu2 = new Student(2, "段誉", 94, 34, 46);
            Student stu3 = new Student(3, "虚竹", 99, 99, 99);
            list.Add(stu1);
            list.Add(stu2);
            list.Add(stu3);
            double sum1 = 0;
            foreach (Student stu in list)
            {
                sum1 += stu.Score1;
            }
            double sum2 = 0;
            foreach (Student stu in list)
            {
                sum2 += stu.Score2;
            }
            double avg = sum2 / list.Count;
            list.Sort();
            Student stu4 = (Student)list[0];
            Console.WriteLine("语文的总分为" + sum1);
            Console.WriteLine("数学的平均分为" + avg);
            Console.WriteLine("英语最高分的姓名为" + stu4.Name);
            Console.ReadLine();
        }
    }
}
