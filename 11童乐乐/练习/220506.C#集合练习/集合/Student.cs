﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 集合
{
    internal class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Score1 { get; set; }
        public double Score2 { get; set; }
        public double Score3 { get; set; }
        public Student(int id, string name, double score1, double score2, double score3)
        {
            this.Id = id;
            this.Name = name;
            this.Score1 = score1;
            this.Score2 = score2;
            this.Score3 = score3;
        }

        public int CompareTo(object obj)
        {
            Student stu = obj as Student;
            if (stu != null)
            {
                return (int)(stu.Score3 - this.Score3);
            }
            else
            {
                return 0;
            }
        }
    }
}
