﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 集合2
{
    internal class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
        public Employee(int id, string name, decimal salary)
        {
            this.Id = id;
            this.Name = name;
            this.Salary = salary;
        }
        public override string ToString()
        {
            Console.WriteLine("姓名：{0} 工资：{1}", Name, Salary);
            return null;
        }
    }
}
