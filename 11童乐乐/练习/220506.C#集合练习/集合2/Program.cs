﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace 集合2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();
            Employee emp1 = new Employee(5, "刘虎", 1000);
            Employee emp2 = new Employee(20, "郑桀", 2000);
            Employee emp3 = new Employee(32, "魏嗔", 3000);
            Employee emp4 = new Employee(45, "罗心", 4000);
            hashtable.Add(emp1.Id, emp1);
            hashtable.Add(emp2.Id, emp2);
            hashtable.Add(emp3.Id, emp3);
            hashtable.Add(emp4.Id, emp4);
            Console.WriteLine("请输入员工号");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(hashtable[id]);
            Console.ReadLine();
        }
    }
}
