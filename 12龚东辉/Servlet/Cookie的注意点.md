## Cookie的注意点

1.Cookie保存在当前浏览器中
	cookie不能跨浏览器，信息只保存在本机中

2.Cookie存中文问题
	Cookie中不能出现中文,不支持中文(不建议cookie存中文)
有中文则需要通过URLEncoder.encode()进行编码然后获取的时候通过URLDecoder.decode()解码

3.Cookie同名问题
	服务器发送重复的Cookie那么会覆盖原有的Cookie

4.浏览器的Cookie数量是有限的
	Cookie的存储是有限的大小大概在4KB左右

```java
/**Cookie注意点
 * 1.Cookie只在当前浏览器中有效（不能跨电脑和浏览器)
 * 2.Cookie不能存中文，如果一定要存中文则需要通过URLEncoder进行编码，然后 通过URLDecoder进行解码
 * 3.出现同名的cookie会覆盖
 * 4.Cookie的存储数量是有上限的，不同浏览器上限不同，大小在4kb左右
 * @author Niann
 */
```

