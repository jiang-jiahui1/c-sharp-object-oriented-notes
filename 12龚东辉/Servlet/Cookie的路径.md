Cookie的setPath() 设置cookie的路径，这个路径直接决定了服务器的请求是否会从浏览器中加载某些cookie

```java
  /*  1.当前服务器任何项目的资源都可以获取cookie对象*/
        Cookie cookie01 = new Cookie("cookie01","cookie01");
        cookie01.setPath("/");//表示任意项目
        resp.addCookie(cookie01);


        /*2.当前项目下的资源才能访问到（默认不设置Cookie的path）*/
        Cookie cookie02 = new Cookie("cookie02","cookie02");
        //默认不设置Cookie的path,或设置当前站点名
//        cookie02.setPath("/");
        resp.addCookie(cookie02);


        /* 3.指定项目下的资源才能访问cookie对象*/
        Cookie cookie03 = new Cookie("cookie03","cookie03");
        //设置指定站点名
        cookie03.setPath("/sc03");
        resp.addCookie(cookie03);



        /*  4.指定目录下的资源可以访问cookie对象*/
        Cookie cookie04 = new Cookie("cookie04","cookie04");
        //设置指定站点名
        cookie04.setPath("/sc04/coo02");
        resp.addCookie(cookie04);
```
