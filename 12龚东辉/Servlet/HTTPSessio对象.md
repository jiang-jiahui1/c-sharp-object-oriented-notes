## HttpSession------------<u>会话</u>

HttpSession对象是javax.servlet.http.HttpSession的实例
作用：标识一次会话或者说确认一个用户 并且在一次**会话**中共享数据,
通过request.getSession()方法获取session对象

```java
/**
 * reuqest.getSession();
 * session的常用方法
 * 获取session对象时会先判断Session对象是否存在，如果存在则获取，如果不存在则创建
 */
//获取session对象
HttpSession session = request.getSession();

//获取session的会话标识符
String id=session.getId();
System.out.println(id);
//获取Session的创建时间
System.out.println(session.getCreationTime());
//获取最后一次访问时间
System.out.println(session.getLastAccessedTime());
//判断是否是新的session对象
System.out.println(session.isNew());
```