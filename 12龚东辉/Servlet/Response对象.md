7.HttpServletResponse对象
Web服务器收到客户端的http请求，会针对每一次请求，分别创建一个用于代表请求的 request 对象和代表
响应的response 对象。
request 和 response 对象代表请求和响应：获取客户端数据，需要通过 request 对象；向客户端输出数据，
需要通过response 对象。
HttpServletResponse 的主要功能用于服务器对客户端的请求进行响应，将 Web 服务器处理后的结果返回给
客户端。service(方法中形参接收的是 HttpServletResponse 接口的实例化对象，这个对象中封装了向客户端发
送数据、发送响应头，发送响应状态码的方法。