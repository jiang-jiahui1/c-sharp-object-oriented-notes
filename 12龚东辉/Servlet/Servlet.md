```java
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/ser01")
public class Servle01 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url=req.getRequestURI()+"";
        System.out.println("获取请求时的完整路径："+url);

        String uri=req.getRequestURI();
        System.out.println("获取请求时的部分路径："+uri);

        String queryString=req.getQueryString();
        System.out.println("获取请求时的参数字符串："+queryString);

        String method= req.getMethod();
        System.out.println("获取请求方式："+method);

        String prototol= req.getProtocol();
        System.out.println("获取当前协议版本："+prototol);

        String webapp=req.getContextPath();
        System.out.println("获取站点名"+webapp);


//        /获取请求的参数
        //获取指定名称的参数值（重点！）只返回字符串
        String uname=req.getParameter("uname");
        String pwd=req.getParameter("pwd");
        System.out.println("uname："+uname+"\tpwd："+pwd );

        //获取指定名称的参数返回字符串数组（复选框)
        String [] hobbys=req.getParameterValues("hobby");
        //非空判断
        if(hobbys!=null && hobbys.length>0){
            for (String hobby:hobbys) {
                System.out.println("爱好："+hobby);

            }
        }



    }
}
```