## Cookie到期时间

​	负整数：表示不存储该cookie，默认-1表示只会在浏览器的内存中存活,关闭浏览器就失效
​	正整数：若大于零表示存储的秒数,将Cookie保存在硬盘里,关闭电脑也能存活，但换浏览器不行
​	0:若为0表示删除该cookie setMaxAge(0)删除cookie

```java
//到期时间:负整数  (默认值是-1表示只在浏览器内存中存活，关闭浏览器失效)
        Cookie cookie1 = new Cookie("uname1","蔡徐坤");
        cookie1.setMaxAge(-1);//关闭浏览器失效
        //响应cookie
        resp.addCookie(cookie1);


        //到期时间 正整数：(表示存活指定秒数,会将数据存在磁盘中)
        Cookie cookie2 = new Cookie("uname2","打篮球");
        cookie2.setMaxAge(30);//存活30秒
        //响应cookie
        resp.addCookie(cookie2);

        //到期时间：0：（表示删除Cookie）
        Cookie cookie3 = new Cookie("uname3","Rap");
        cookie3.setMaxAge(0);//删除cookie
        //响应cookie
        resp.addCookie(cookie3);
```

