```properties
#properties格式

#更改服务器端口
server.port=8082

#编写自定义配置项，并在项目中通过@Value直接注入 
test.name="Hello World！"
```

```yaml
#yaml格式
server:
  post:8082
#编写自定义配置项，并在项目中通过@Value直接注入 
test:
  name: "Hello world"
```





在项目中通过@Value直接注入

```java
@Value("${test.name}")
String name;//直接将test.name的文件注入到name里面

@RequestMapping("/index")
@ResponseBody
public String index(){
    return  "欢迎访问！"+name;
}
```