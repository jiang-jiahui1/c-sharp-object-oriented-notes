# SpringSecurity是什么

spring security 的核心功能主要包括：

- 认证 （你是谁）

- 授权 （你能干什么）

- 攻击防护 （防止伪造身份）

  

   

   其核心就是一组过滤器链，项目启动后将会自动配置。最核心的就是 Basic Authentication Filter 用来认证用户的身份，一个在spring security中一种过滤器处理一种认证方式。

![img](https://img-blog.csdnimg.cn/20190116102342618.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIyMTcyMTMz,size_16,color_FFFFFF,t_70)

对于username password认证过滤器来说， 会检查是否是一个登录请求；是否包含username 和 password （也就是该过滤器需要的一些认证信息） ；

如果不满足则放行给下一个。

   下一个按照自身职责判定是否是自身需要的信息，basic的特征就是在请求头中有 Authorization:Basic eHh4Onh4 的信息。中间可能还有更多的认证过滤器。**最后一环是 FilterSecurityInterceptor**，这里会判定该请求是否能进行访问rest服务，判断的依据是 BrowserSecurityConfig中的配置，如果被拒绝了就会抛出不同的异常（根据具体的原因）。Exception  Translation Filter 会捕获抛出的错误，然后根据不同的认证方式进行信息的返回提示。

注意：绿色的过滤器可以配置是否生效，其他的都不能控制。



## 整合SpringSecurity依赖

在XML文件导入SpringSecurity依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

我们可以在配置文件中直接配置密码：

```yaml
spring:
  security:
    user:
      name: admin #用户名
      password: 123456 #密码
      roles:  #角色
        - user
        - admin
```





页面控制和数据库校验我们还需要提供 WebSecurityConfigurerAdapter 的实现类去完成

```java
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    protected  void configure(HttpSecurity http) throws Exception{
            http
                    .authorizeRequests()//授权请求
                    .antMatchers("/login").permitAll()
                    .anyRequest().hasRole("admin")
                    .and()
                    .formLogin();//登录窗口
    }
}

```



SpringBoot：需要什么功能只需要导入对应的Starter依赖就可以了