# 配置index



springBoot是自动扫描的，因此我们直接创建一个Controller可以直接被加载

```java
@Controller
public class Hello {
    //可以直接访问
        @RequestMapping("/index")
        @ResponseBody
        public String index(){
            return  "欢迎访问！";
        }
}
```



Ta还可以自动识别类型，如果我们返回一个对象的数据类型，那么他会自动帮我们转换成JSON数据类型格式，无需配置。

```java
@Data
public class Student {
    int sid;
    String Name;
    String Sex;
}

		//Student类
        @RequestMapping("/student")
        @ResponseBody
        public Student  student(){
            Student s1=new Student();
            s1.Name="张三";
            s1.Sex="男";
            s1.sid=18;
            return s1;
        }
```

最后浏览器可以直接得到  application/json 的相应数据

输出：
![image-20220418213734510](C:\Users\gdhlo\AppData\Roaming\Typora\typora-user-images\image-20220418213734510.png)