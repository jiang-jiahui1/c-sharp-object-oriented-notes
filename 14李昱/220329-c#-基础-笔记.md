输出

​      

```c#
Console.Write("");//不换行 print

​      Console.WriteLine("Hello World!");//自动换行 println

​      Console.ReadLine();
```





​      输入

​      

```c#
Console.WriteLine("请输入一个数字");

​      byte short16位(Bit) int(32) long(64) 

​      int a = Convert.ToInt32(Console.ReadLine());
```



​      变量



​      

```c#
有符号(-32100,)，

​      short b = -32768;//-32768-32767

​      无符号(0 -)

​      ushort b2 = 65000;

​      String 类，

​      string a = "abc";

​      bool b1 = true;

​      decimal money = 3.0M;
```

​      数据类型:整型，单精度float，双精度double，字符char,bool，string，decimal



​      运算符

​      算数：+,-,/，%

​     

```c#
 Console.WriteLine(5 / 3);

​      赋值：=,+=,-=,*=,/=,%=

​      var a1= 1.0M;

​      Type a1_type = a1.GetType();

​      Console.WriteLine(a1_type);
```



​      

```c#
比较，逻辑：>,<,>=,<=,!=,==, &&, ||, !

​      &&:  true && true --> true 

​      || :  false || false --> false

​      短路与： && false && true(false)  -->  false

​      短路或：|| true || true(false) --> true
```

