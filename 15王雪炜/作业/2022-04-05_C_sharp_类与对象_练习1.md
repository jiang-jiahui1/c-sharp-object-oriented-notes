# 作业一：

#### 1、创建一个控制台应用程序，该程序名为ClassTest,在该程序中创建一个以Role为类名的、包含以下成员的一个类。

#### 角色名、性别、出处、地位、门派地位、爱侣、父亲、母亲：

#### 2、在ClassTest程序中，在Role类中定义一个该类的构造函数，实现给该类的字段赋值的功能。

#### 3、在ClassTest程序主函数中创建一个对象，并完成给该对象的成员赋以下的值并完成输出：张无忌、男、倚天屠龙记、主角、明教教主、赵敏、张翠山、殷素素

```c#
class Role
    {
        public string name;
        public string gender;
        public string provenance;
        public string status;
        public string status_sect;
        public string wife;
        public string father;
        public string mother;

        public Role() { 
        
        }

        public void Output() {
            Console.WriteLine("名字:{0}、性别:{1}、出处:{2}、地位:{3}、" +
                "门派地位:{4}、爱侣:{5}、父亲:{6}、母亲:{7}", name,gender,provenance,
                status,status_sect,wife,father,mother); 
        }
    }
    
 class Program
    {
        static void Main(string[] args)
        {
            Role ro = new Role();
            ro.name = "张无忌";
            ro.gender = "男";
            ro.provenance = "倚天屠龙记";
            ro.status = "主角";
            ro.status_sect = "明教教主";
            ro.wife = "赵敏";
            ro.father = "张翠山";
            ro.mother = "殷素素";
            ro.Output();
        }
    }
```

---

# 作业二：

#### 用于模拟银行账户（Account类），要求有初始化余额（balance），存取现金操作和查看余额的操作

#### 初始化余额有两种方式，默认余额（1000）和客户指定余额

```c#
class Account
    {
        public decimal starbalance;//初始金额
        public decimal balance;//余额存储
        public decimal saveMoney;//
        public decimal loadMoney;

        public Account(decimal Starbalance,decimal Balance,decimal SaveMoney,decimal LoadMoney) {
            this.starbalance = Starbalance;
            this.balance = Balance;
            this.saveMoney = SaveMoney;
            this.loadMoney = LoadMoney;
            
            balance += saveMoney;
            balance -= loadMoney;
        }

        public void Output() {
            Console.WriteLine("初始余额："+starbalance);
            Console.WriteLine("存钱："+saveMoney);
            Console.WriteLine("取钱：" + loadMoney);
            Console.WriteLine("余额:"+balance);
        }
    }
    
 class Program
    {
        static void Main(string[] args)
        {
            Account ac = new Account(1000M,0M,600M,100M);
            ac.Output();
        }
    }
```

