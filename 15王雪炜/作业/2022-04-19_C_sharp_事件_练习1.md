练习一：

```c#
using System;

namespace TEST_002
{
    public delegate void Myalarm(int temperature); //报警 
    public delegate void Mydisplay(int temperature); //显示
    class Heater //热水器
    {
        int _temperature;
        public Heater(int Temperature){
            this._temperature=Temperature;
        }
        public int Temperature {
            get { return _temperature; }
            set
            {
                if ((_temperature >= 0)&&(_temperature <= 100))
                {
                    this._temperature = value;
                }
                else
                {
                    Console.WriteLine("温度异常");
                    
                }
            }
        }

        public event Myalarm Doalarm;
        public event Mydisplay Dodisplay;

        public void WaterDisplay(int temperature) {
            if (this._temperature > 95)
            {
                Doalarm(this._temperature);
                Dodisplay(this._temperature);
                Console.WriteLine("水温为：{0},已经烧开了",Temperature);
            }
            else
            {
                Console.WriteLine("水温为：{0},未烧开了", Temperature);
            }
        }
    }

    class Alarm //报警器
    {
        public void MakeAlarm(int temperature) {
            Console.WriteLine("叮咚，叮咚，叮咚，现在水温是：{0}",temperature);
        }
    }

    class Display  //显示屏
    {
        public void ShowMsg(int temperature) {

            Console.WriteLine("显示器显示当前温度为：{0}",temperature);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Heater h1 = new Heater(96);

            Alarm al1 = new Alarm();
            Display dis1 = new Display();

            h1.Doalarm += al1.MakeAlarm;
            h1.Dodisplay += dis1.ShowMsg;
            h1.WaterDisplay(96);
        }
    }
}

```

