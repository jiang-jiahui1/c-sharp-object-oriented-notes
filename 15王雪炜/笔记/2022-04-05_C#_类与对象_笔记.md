## 类和对象

#### 		定义：世间万物的任何一个东西，特征，行为，想法等都可以定义一起都能叫做类。（类是个抽象的代表）

```c#
class Person{
}

class 类名{
	代码块
}
```

#### 		定义：类里面所有的每一个特征，行为，等都是一个个对象。

```c#
class Person{
	public(修饰符) string(数据类型) id(对象名);
	public string id;
}
```

---





## 构造方法

#### 		定义：实例化一个对象，是一个特殊的方法必须要在实例化对象时使用。

```c#
class 类名{
	public(修饰符) 类名(){
		代码块；
	}
}

无参：
	class Atifck{
	public Atifck(){
	}
}

有参：
	class Atifck{
	public Atifck(int a,int b){
		this.A=a;
		this.B=b;
	}
}
```

#### P,S: 构造方法的命名要和类名一致，且没有返回值（void）

---



## this关键字

#### 		定义：this的英文意思是“这个，当前”,在C#中是当前类的对象，可以使用this语法给对象赋值

```c#
class 类名{
	public 结构方法名(有参参数){
		this.对象名 = 赋值对象；
	}
}

class Atifck{
	public Atifck(int a,int b){
		this.A=a;
		this.B=b;
	}
}
```

---



### 命名空间：

#### 				就是一个个类项目组成的文件夹的空间可以导入和导出
