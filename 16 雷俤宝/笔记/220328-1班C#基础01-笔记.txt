输出
      Console.Write("") 同与 Java print 输出不换行
      Console.WriteLine("") 同与 java println 自动换行
输入
      Console.WrinteLine("输入数字")；
      int a = Convert.ToInt32(Console.ReadLine())
      数字字节长度
            byte short（Bit）16位   （int）32位   （long）64位
数据类型
      整型int，单精度float，双精度double，字符char，布尔型bool，字符串string，小数decimal
运算符
      算数
      +， -，  *，  /， %
      加，减，乘，除，余

      赋值
      =，+=，-=，*=，/=，%=

      比较
      >,  <,  >=,  <=,  !=,  ==

      逻辑
      &&,||,!
            &&:   true && true --> true 
            ||    :  false || false --> false
            短路与： && false && true(false)  -->  false
            条件一不符合，跳过条件二，结果错误
            短路或：|| true || true(false) --> true
            条件一符合，跳过条件二，结果正确

      三元 ?:
      (条件表达式-->true,false)?(true):(false)
      int c = 5          int a = 6
      int max = c > a ? c : a
      max = 6
选择结构
      if(表达式)
      {
      代码块 
      }
      else
      {
      代码块
      }

嵌套


      if(表达式)
    {
           代码块 1
    }
      else if
    {
           代码块 2
    }
      else if
    {
           代码块 3
    }
      else
    {
           代码块 4
    }
循环结构
    for（表达式1;表达式2;表达式3）
    {
    代码块
    }