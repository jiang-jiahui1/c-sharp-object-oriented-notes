﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0502作业
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string txt = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            int n = 0;
            int m = 0;
            foreach (char ws in txt)
            {
                if (ws == '类')
                {
                    n++;
                }
            }
            foreach (char ws in txt)
            {
                if (ws == '码')
                {
                    m++;
                }
            }
            Console.WriteLine("类的个数"+n);
            Console.WriteLine("码的个数"+m);

            string t1 = txt.Replace("类","");
            string t2 = txt.Replace("码","");
            Console.WriteLine("类的个数" + (txt.Length - t1.Length));
            Console.WriteLine("码的个数" + (txt.Length - t2.Length));
            
            string[] t11 = txt.Split('类');
            string[] t22 = txt.Split('码');
            Console.WriteLine("类的个数" + (t11.Length-1));
            Console.WriteLine("码的个数" + (t22.Length-1));

            string txt2 = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            string[] t21 = txt2.Split(' ');
            txt2 = txt2.Replace(" ", "");
            Console.WriteLine(txt2);
            Console.WriteLine("空格数为"+(t21.Length-1));

            string[] t31 = new string[4];
            string[] t32 = {"姓名","年龄","家庭住址","兴趣爱好" };
            for (int i = 0; i < t31.Length; i++)
            {
                Console.WriteLine("数入你的{0}",t32[i]);
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < t32.Length; i++)
            {
                sb.Append(t32[i]);
            }
            Console.WriteLine(sb);
            Console.ReadKey();
        }   

    }
}
