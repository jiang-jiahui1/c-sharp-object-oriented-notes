﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0502常用类
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //1.	生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
            Console.WriteLine("1.生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。");
            Random r = new Random();
            int[] sj = new int[10];
            for (int i = 0; i < sj.Length; i++)
            {
                sj[i] = Convert.ToInt32(r.Next(1,11).ToString());
            }
            foreach (var item in sj)
            {
                Console.WriteLine(item);
            }

            //2.	生成0-5之间的随机小数，保留两位小数。
            Console.WriteLine("2.生成0-5之间的随机小数，保留两位小数。");
            double n =r.NextDouble()*5;
            Console.WriteLine(n);
            double m = Math.Round(n, 2);
            Console.WriteLine(m);
            

            //3.	生成4-7之间的随机小数，保留两位小数。
            Console.WriteLine("3.生成4-7之间的随机小数，保留两位小数。");
            n = (r.NextDouble() * 3) + 4;
            Console.WriteLine(n);
            m = Math.Round(n, 2);
            Console.WriteLine(m);


            Console.WriteLine("输出当前系统时间，分别获取当前日期，时分秒，再拼接输出。");
            int nf = DateTime.Now.Year;
            int yf = DateTime.Now.Month;
            int rq = DateTime.Now.Day;
            int s=DateTime.Now.Hour;
            int f=DateTime.Now.Minute;
            int g=DateTime.Now.Second;
            Console.WriteLine("{0}/{1}/{2} {3}:{4}:{5}",nf,yf,rq,s,f,g);


            Console.WriteLine("本周天和本周六的日期");
            DateTime dqrq = DateTime.Now;
            int no = (int)dqrq.DayOfWeek;
            DateTime zt = dqrq.AddDays(-no);
            Console.WriteLine("本周日于：" + zt.ToString("yyyy-MM-dd"));            
            DateTime zl = dqrq.AddDays(+(6 - no));
            Console.WriteLine("本周六于：" + zl.ToString("yyyy-MM-dd"));


            Console.ReadKey();




        }
    }
}
