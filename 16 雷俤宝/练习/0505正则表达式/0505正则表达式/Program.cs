﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace _0505正则表达式
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //用户输入邮箱，请验证其合法性。
            Console.WriteLine("请输入您的邮箱：");
            string email = Console.ReadLine();
            bool a = Regex.IsMatch(email, @"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
            Console.WriteLine(a);


            //用户输入手机号码，请验证其合法性。
            Console.WriteLine("请输入电话号码");
            string telephone = Console.ReadLine();
            bool b = Regex.IsMatch(telephone, @"^(((13[0-9]{1})|(15[0-35-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$");
            Console.WriteLine(b);


            string c = "来自13145698125的高中同学说今晚他和另一位同学来我家吃晚餐。同事+8613879541685说她孩子生病了请我帮忙完成她的工作，还说请我打13549643259这个电话跟她领导说一声";
            MatchCollection results = Regex.Matches(c, @"(\+86)?1\d{10}");
            foreach (Match item in results)
            {

                Console.WriteLine("下标：{0}，值：{1}", item.Index, item.Value);
            }


            //使用Regex实现“123abc456ABC789”中每一个数字替换成“X”。
            string d = "123abc456ABC789";
            Regex regex = new Regex("(1|2|3|4|5|6|7|8|9|0)", RegexOptions.IgnoreCase);
            string NewStr = regex.Replace(d, "X");
            Console.WriteLine(d);
            Console.WriteLine(NewStr);


            Console.ReadKey();
        }
    }
}
