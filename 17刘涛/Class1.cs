﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class gy
    {
        //        雇员系统，定义雇员基类，共同的属性，姓名、地址和出生日期(可有可无)，
        //子类：程序员，秘书，高层管理，清洁工，
        //他们有不同的工资算法，其中高级主管和程序员采用底薪加提成(6000)的方式，
        //高级主管和程序员的底薪分别是5000元和2000元?，秘书和清洁工采用工资的方式，
        //工资分别是3000和1000，（以多态的方式处理程序。）

        public string name { get; set; }
        public string dz { get; set; }
        public decimal dixin { get; set; }



        public virtual decimal salay() {
            return dixin;
        }
    }
    class cxy : gy {
        public decimal tc = 6000;
        public new decimal dixin = 2000;
        public override decimal salay()
        {
            return dixin + tc;
        }

    }
    class gl : gy {
        public decimal tc = 6000;
        public new decimal dixin = 5000;
        public override decimal salay()
        {
            return dixin + tc;
        }


    }
    class ms : gy {
        public decimal dixin = 3000;
        public override decimal salay()
        {
            return dixin;
        }


    }
    class qjg : gy {

        public decimal dixin = 1000;
        public override decimal salay()
        {
            return dixin;
        }
    }

}
