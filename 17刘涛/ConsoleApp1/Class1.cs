﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class student
    {
        //一、	创建一个Student类，类中包含三个公共成员变量：stuName(学生姓名)，stuAge(学生年龄)，stuSex(学生性别)，一个静态成员变量tcAge(共青团员退团年龄)，定义一个构造函数用于初始化学生姓名，学生年龄，学生性别，再定义一个无参构造函数，再定义一个静态的方法SayHello()，输出学生的所有信息，实例化一个对象Mary
        public string name;
        public int age;
        public string sex;
        public static int tcage;

        public student(string name, int age, string sex)
        {
            this.name = name;
            this.age = age;
            this.sex = sex;
        }
        public student()
        {

        }
        public static void sayhello() {
            student mary = new student("张明", 18, "男");
            Console.WriteLine("姓名{0}，年龄{1}，性别{2}，退团年龄{3}", mary.name, mary.age, mary.sex, student.tcage);
        }
        static void Main(string[] args)
        {
            sayhello();
        }




    }
    }
    
