﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    //二、	创建一个Math类，里边定义两个静态的方法，一个用于求两个数的和，另一个用于求三个数的和，方法名都定义成Add()（利用方法的重载）
    class math
    {
        public static int add(int a, int b)
        {
            return a + b;
        }
        public static int add(int a, int b,int c)
        {
            return a + b+c;
        }
    }
}
