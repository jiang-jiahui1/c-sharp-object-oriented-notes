﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    //三、	编写一个程序，用于计算三个职员的工资。第一位职员默认的基本工资为1000元，第二位职员除具有基本工资外，还具有住房津贴。接受用户输入的基本工资和住房津贴。第三位职员可能是经理也可能不是，如果是，则有奖金收入，应接受输入的奖金值。奖金应加到基本工资内（提示：创建一个Employee类，类中创建一个ComputeSalary()的方法，为每个不同类别的职员重载该方法）
    class Employee
    {
        public static int computesalary(int a,int b) {
            a = 1000;
            return a + b;
        
        }
        public static int computesalary(int a, int b,int c)
        {
            a = 1000;
            return a + b+c;

        }
    }
}
