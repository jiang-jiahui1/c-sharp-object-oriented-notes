﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Operation
    {
        //        将加减乘除四则运算看作一种操作,请设计操作抽象类Operation，
        //该抽象类有一个Op的方法计算两个整数相应运算，并返回计算结果。
        //并派生出四则运算操作类。计算器一个方法Do方法
        public virtual double op(double a,double b) {
            return 0;
        }
    }
    class jia:Operation {

        public override double op(double a, double b)
        {
            return a+b;
        }
    }
    class jian : Operation
    {

        public override double op(double a, double b)
        {
            return a - b;
        }
    }
    class c : Operation
    {

        public override double op(double a, double b)
        {
            return a * b;
        }
    }
    class chu : Operation
    {

        public override double op(double a, double b)
        {
            return a / b;
        }
    }

}
