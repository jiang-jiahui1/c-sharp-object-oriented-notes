﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Shape
    {
        //        创建一个Shape类,此类包含一个名为color的数据成员(用于存放颜色值)和一个
        //GetColor方法(用于获取颜色值)，这个类还包含一个名为GetArea的虚方法。
        //用这个类创建名为Circle和Square的两个子类，这两个类都包含两个数据成员，
        //即radius和sideLen。这些派生类应提供GetArea方法的实现，以计算相应形状的
        //面积。

        public string color;
        public string GetColor () {
            return color;
        }
        public virtual double GetArea (){

            return 0;
        }
    }
    class Circle:Shape {
        public double radius { get; set; }
        public override double GetArea()
        {
            return radius * radius * 3.14;
        }

        public Circle(double radius) {
            this.radius = radius;
        }

    }
    class Square:Shape {
        public double sideLen { get; set; }
        public override double GetArea()
        {
            Console.WriteLine("赋值结果{0}",sideLen);
            return sideLen * sideLen;
        }
        public Square(double sideLen) {
            Console.WriteLine("闯入的参数{0}", sideLen);
            this.sideLen = sideLen;
        }

    }
}
