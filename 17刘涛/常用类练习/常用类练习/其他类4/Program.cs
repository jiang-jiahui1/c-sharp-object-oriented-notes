﻿using System;

namespace 其他类4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //4.	输出当前系统时间，分别获取当前日期，时分秒，再拼接输出。

            DateTime bt = DateTime.Now;
            Console.WriteLine(bt);

            string a = DateTime.Now.ToShortDateString().ToString();
            string b = DateTime.Now.ToLongTimeString().ToString();
            Console.WriteLine(a+" "+b);
        }
    }
}
