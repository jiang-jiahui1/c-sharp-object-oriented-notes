﻿using System;

namespace 常用类练习
{
    //与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。
    internal class Program
    {
        static void Main(string[] args)
        {
            //使用循环
            string s = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            //int sum1 = 0;//统计类的个数
            //int sum2 = 0;//统计码的个数

            //for (int i = 0; i < s.Length; i++)
            //{
            //    if (s[i].Equals('类'))
            //    {
            //        sum1 = sum1+1;
            //    }

            //    if (s[i].Equals('码'))
            //    {
            //        sum2 = sum2+1;
            //    }
            //}

            //Console.WriteLine("类的个数为{0}", sum1);
            //Console.WriteLine("码的个数为{0}",sum2);

            //使用Replace
            //string a = s.Replace('类','1');
            //string b = s.Replace('码','2');

            //int sum1 = 0;//统计类次数
            //int sum2 = 0;//统计码次数

            //for (int i = 0; i < a.Length; i++)
            //{
            //    if (a[i] == '1')
            //    {
            //        sum1 = sum1 + 1;
            //    }
            //}

            //for(int i = 0; i < b.Length; i++)
            //{
            //    if (b[i] == '2')
            //    {
            //        sum2 = sum2 + 1;
            //    }
            //}

            //Console.WriteLine("类的个数为"+sum1);
            //Console.WriteLine("码的个数为"+sum2);

            //使用Split
            string[] a = { "类" };
            string[] b = { "码" };

            string[] s1 = s.Split(a, StringSplitOptions.RemoveEmptyEntries);
            string[] s2 = s.Split(b, StringSplitOptions.RemoveEmptyEntries);

            Console.WriteLine("类的次数为"+(s1.Length-1));
            Console.WriteLine("码的次数为"+(s2.Length-1));


        }
    }
}
