﻿using System;
using System.Text;
namespace 常用类练习3
{
    internal class Program
    {
        //三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder 类把这些信息连接起来并输出。
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();

            Console.WriteLine("请输入你的姓名");
            string name = Console.ReadLine();

            Console.WriteLine("请输入你的年龄");
            string age = Console.ReadLine();

            Console.WriteLine("请输入你的家庭住址");
            string add = Console.ReadLine();

            Console.WriteLine("请输入你的兴趣");
            string abb = Console.ReadLine();

            sb.Append(name);
            sb.Append(age);
            sb.Append(add);
            sb.Append(abb);
            Console.WriteLine(sb);

        }
    }
}
