﻿using System;
using System.Text.RegularExpressions;

namespace 正则表达式3
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.从“来自13145698125的高中同学说今晚他和另一位同学来我家吃晚餐。同事 + 8613879541685说她孩子生病了请我帮忙完成她的工作，还说请我打13549643259这个电话跟她领导说一声”该段话中提取电话号码。匹配规则参考：（\+86）?1\d{ 10}
            string F = "来自13145698125的高中同学说今晚他和另一位同学来我家吃晚餐。同事+8613879541685说她孩子生病了请我帮忙完成她的工作，还说请我打13549643259这个电话跟她领导说一声”";

            MatchCollection a = Regex.Matches(F, @"(\+86)?1\d{10}");
            Console.WriteLine("符合条件的：");
            foreach (Match i in a)
            {
                Console.WriteLine(i);
            }
        }
    }
}
