﻿using System;

namespace convert_类型转换
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //string s = "123";
            //double d = Convert.ToDouble(s);
            //int n = Convert.ToInt32(s);
            //Console.WriteLine(n);
            //Console.WriteLine(d);
            //Console.ReadKey();

            //让用户输入姓名 语文 数学 英语 三门课的成绩
            //然后给用户显示 ：xx,你的总成绩为xx分，平均成绩为xx分
            Console.WriteLine("请输入您的姓名");
            string name = Console.ReadLine();
            Console.WriteLine("请输入您的语文成绩");
            string strChinese = Console.ReadLine();
            Console.WriteLine("请输入您的数学成绩");
            string strMath = Console.ReadLine();
            Console.WriteLine("请输入您的英语成绩");
            string strEnglish = Console.ReadLine();
            int chinese = Convert.ToInt32(strChinese);
            int math = Convert.ToInt32(strMath);
            int english = Convert.ToInt32(strEnglish);
            int sum = chinese + math + english;
            int avg = sum / 3;
            Console.WriteLine("{0}你的总成绩是{1}，平均成绩是{2:0.00}", name, sum, avg);

            //Console.WriteLine();
            //int number = Convert.ToInt32(Console.ReadLine());
        }
    }
}
