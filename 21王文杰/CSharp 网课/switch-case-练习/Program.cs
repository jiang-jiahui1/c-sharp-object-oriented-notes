﻿using System;

namespace switch_case_练习
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个考试成绩：");
            int score=Convert.ToInt32(Console.ReadLine());
            

            #region MyRegion
            switch (score / 10)
            {
                case 10://case 10 要执行的跟case 9 一样
                case 9:
                    Console.WriteLine("A");
                    break;
                case 8:
                    Console.WriteLine("B");
                    break;
                case 7:
                    Console.WriteLine("C");
                    break;
                case 6:
                    Console.WriteLine("D");
                    break;
                default:
                    Console.WriteLine("E");
                    break;
            } 
            #endregion
            Console.ReadKey();
        }
    }
}
