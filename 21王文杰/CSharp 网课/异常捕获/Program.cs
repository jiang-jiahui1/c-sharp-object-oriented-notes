﻿using System;

namespace 异常捕获
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //先自减再参与运算
            //int number = 10;
            //int result = 10 + --number; 
            //Console.WriteLine(result);
            //Console.WriteLine(number);
            //Console.ReadKey();

            //变量的作用域
            //变量的作用域就是你能使用到这个变量的范围
            //就是你声明这个变量的大括号到这个大括号结束的范围

            //try-catch
            //语法
            //try
            //{
            //    可能出现异常的代码；
            //}
            //catch
            //{
            //    出现异常后要执行的代码；
            //}


            #region 异常捕获（1.0）
            //bool b=true;
            //int number = 0;
            //Console.WriteLine("请输入一个值");
            //try
            //{
            //    number=Convert.ToInt32(Console.ReadLine());

            //}
            //catch
            //{
            //    Console.WriteLine("输入的内容不符合：");
            //    b=false;
            //}

            //if(b)
            //{
            //    Console.WriteLine(number*2);
            //}
            //Console.ReadKey();
            #endregion

            #region 异常捕获（2.0）
            //int number = 0;
            //Console.WriteLine("请输入一个值");
            //try
            //{
            //    number=Convert.ToInt32(Console.ReadLine());
            //    Console.WriteLine(number * 2);

            //}
            //catch
            //{
            //    Console.WriteLine("输入的内容不符合：");
            //}
            //Console.ReadKey();
            #endregion

        }
    }
}
