﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work3
{
    class Program
    {
        //三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder 类把这些信息连接起来并输出。
        static void Main(string[] args)
        {
            string[] name = new string[4];
            string[] test = { "姓名", "年龄", "家庭住址", "兴趣爱好" };
            for (int i = 0; i < name.Length; i++)
            {
                Console.WriteLine("请输入你的{0}：", test[i]);
                name[i] = Console.ReadLine();
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < name.Length; i++)
            {
                sb.Append(name[i]);
            }
            Console.WriteLine(sb);
            Console.ReadKey();
        }
    }
}
