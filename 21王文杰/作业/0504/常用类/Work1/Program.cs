﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work1
{
    class Program
    {
        //1.	生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
        static void Main(string[] args)
        {
            Random ra = new Random();
            int[] arr = new int[10];
            for (int i = 0; i <arr.Length; i++)
            {
                arr[i] = Convert.ToInt32(ra.Next(1, 11 ).ToString());
            }
            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }

    }
}
