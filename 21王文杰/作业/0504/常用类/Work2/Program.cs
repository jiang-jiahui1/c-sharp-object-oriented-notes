﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work2
{
    class Program
    {
        //2.	生成0-5之间的随机小数，保留两位小数。
        static void Main(string[] args)
        {
            Random ra = new Random();
            double number = ra.NextDouble() * 5;
            Console.WriteLine(number);
            double NewNumber = Math.Round(number, 2);
            Console.WriteLine(NewNumber) ;
            Console.ReadKey();
        }
    }
}
