﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work3
{
    class Program
    {
        //3.	生成4-7之间的随机小数，保留两位小数。
        static void Main(string[] args)
        {
            Random ra = new Random();
            double number = (ra.NextDouble() * 3) + 4;
            Console.WriteLine(number);
            double NewDouble = Math.Round(number, 2);
            Console.WriteLine(NewDouble);
            Console.ReadKey();
        }
    }
}
