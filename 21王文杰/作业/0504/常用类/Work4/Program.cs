﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work4
{
    class Program
    {
        //4.	输出当前系统时间，分别获取当前日期，时分秒，再拼接输出。
        static void Main(string[] args)
        {
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;
            int hour = DateTime.Now.Hour;
            int minute = DateTime.Now.Minute;
            int second = DateTime.Now.Second;
            Console.WriteLine("{0}-{1}-{2} {3}:{4}:{5}",year,month,day,hour,minute,second);
            Console.ReadKey();


        }
    }
}
