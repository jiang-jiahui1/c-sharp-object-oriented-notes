﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work5
{
    class Program
    {
        //5.	本周天和本周六的日期；
        static void Main(string[] args)
        {
            //根据当前日期求本周天日期
            DateTime now = DateTime.Now;
            Console.WriteLine(now.DayOfWeek);
            int week = (int)now.DayOfWeek;
            Console.WriteLine(week);
            DateTime sunday = now.AddDays(-week);
            Console.WriteLine("本周天日期：" + sunday.ToString("yyyy-MM-dd"));
            DateTime satday = now.AddDays(+(6-week));
            Console.WriteLine("本周六日期：" + satday.ToString("yyyy-MM-dd"));
            Console.ReadKey();
        }
    }
}
