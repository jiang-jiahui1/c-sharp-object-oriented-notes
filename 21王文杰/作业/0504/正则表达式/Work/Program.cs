﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace Work
{
    class Program
    {
        //1.	用户输入邮箱，请验证其合法性。
        static void Main(string[] args)
        {
            Console.WriteLine("请输入您的邮箱：");
            string email = Console.ReadLine();
            bool b = Regex.IsMatch(email, @"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
            Console.WriteLine(b);
            Console.ReadKey();
        }
    }
}
