﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Work2
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.	用户输入手机号码，请验证其合法性。
            Console.WriteLine("请输入电话号码");
            string telephone = Console.ReadLine();
            bool b = Regex.IsMatch(telephone, @"^(((13[0-9]{1})|(15[0-35-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$");
            Console.WriteLine(b);
            Console.ReadKey();
        }
    }
}
