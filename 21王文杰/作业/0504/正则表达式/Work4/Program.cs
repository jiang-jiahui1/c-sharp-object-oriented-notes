﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Work4
{
    internal class Program
    {
        //4.使用Regex实现“123abc456ABC789”中每一个数字替换成“X”。
        static void Main(string[] args)
        {
            string str = "123abc456ABC789";
            Regex regex = new Regex("(1|2|3|4|5|6|7|8|9|0)",RegexOptions.IgnoreCase);
            string NewStr = regex.Replace(str, "X");
            Console.WriteLine(str);
            Console.WriteLine(NewStr);
            Console.ReadKey();
        }
    }
}
