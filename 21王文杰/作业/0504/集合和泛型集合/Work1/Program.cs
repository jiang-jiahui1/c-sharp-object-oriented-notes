﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Work1
{
    //输入班级人数，输入每个人的学号，姓名、语，数，英成绩进入集合
    //求语文的总分，数学的平均分，英语的最高分的人的姓名
    //（使用ArrayList）
    //		数据：学号姓名语文数学英语
    //				1	乔峰	85	 42	  67
    //                2	段誉	94	 34	  46
    //                3	虚竹	99	 99	  99

    class Program
    {
        static void Main(string[] args)
        {
            ArrayListDemo2();
            Console.ReadKey();
        }
        static void ArrayListDemo2()
        {
            //创建集合对象，存储多个数据（学生信息）
            ArrayList list = new ArrayList();

            //创建学生对象，存储一个学生的数据
            Student stu1 = new Student(1, "乔峰", 85, 42, 67);
            Student stu2 = new Student(2, "段誉", 94, 34, 46);
            Student stu3 = new Student(3, "虚竹", 99, 99, 99);
            
            //将学生对象添加到集合中
            list.Add(stu1);
            list.Add(stu2);
            list.Add(stu3);

            //实例化一个总语文成绩的数组
            ArrayList TotalChinese = new ArrayList();
            //将语文成绩添加数组内
            TotalChinese.Add(stu1.Chinese);
            TotalChinese.Add(stu2.Chinese);
            TotalChinese.Add(stu3.Chinese);

            //实例化一个总'数学成绩的数组
            ArrayList TotalMath = new ArrayList();
            //将语文成绩添加数组内
            TotalMath.Add(stu1.Math);
            TotalMath.Add(stu2.Math);
            TotalMath.Add(stu3.Math);



            //对学生信息进行排序，需要在学生类中实现IComparable接口
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }

            //初始化一个字段用于填入总语文成绩
            double totalChinese = 0;
            foreach (double tc in TotalChinese)
            {
                totalChinese += tc;
            }
            Console.WriteLine("总语文成绩为："+totalChinese);

            //初始化一个字段用于填入总数学成绩
            double totalMath = 0;
            foreach (double tm in TotalMath)
            {
                totalMath += tm;
            }
            Console.WriteLine("数学平均分为："+Math.Round((totalMath/3.0),2));
            //排序，对ICompareTo接口里面的方法CompareTo实现 other.name-this.name 实现倒序排序
            list.Sort();
            Console.WriteLine("英语最高分是："+((Student)list[0]).Name);
           
        }
    }
}
