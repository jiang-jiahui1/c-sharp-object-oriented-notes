﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work1
{
//    1	乔峰	85	 42	  67
//2	段誉	94	 34	  46
//3	虚竹	99	 99	  99

    class Student:IComparable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Chinese { get; set; }
        public double Math { get; set; }
        public double English { get; set; }
        
        public Student ()
        {

        }
        public Student(int id, string name, double chinese,double math,double english)
        {
            this.Id = id;
            this.Name = name;
            this.Chinese = chinese;
            this.Math = math;
            this.English = english;
        }
        
        
        //重写ToString方法，使输出对象时，可以是我们return的信息
        public override string ToString()
        {
           
            return $"学号：{Id},姓名：{Name}，语文成绩：{Chinese},数学成绩:{Math},英语成绩:{English}";
        }

        public int CompareTo(object obj)
        {
            //as判断对象是否是Student,是返回该对象，不是返回null
             Student other = obj as Student;
            //Id比较
            //this.Id与other.id对比
            if (other != null)
            {
                //根据学号排序
                //return this.Id - other.Id;

                //根据名字排序
                //return string.Compare(this.Name,other.Name);

                //根据成绩排序
                //return (int)(other.Score - this.Score);
                return (int)(other.English - this.English);
            }
            else
            {
                return 0;
            }
        }
    }
}
