﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work2
{
    //    问题描述：
    //员工信息包括员工号（int），员工姓名和薪水，利用员工号作为键对象存储和检索员工信息。
    //问题分析：
    //首先定义一个员工类Employee,然后把员工号作为键对象把员工作为值存储在HashTable中，再根据用户输入的员工号取出对象的值对象。
    //参考步骤：
    //（1）	新建一个名称为HashTableTest的项目
    //（2）	添加一个Employee类
    class Employee
    {
        int ID { get; set; }
        string Name { get; set; }
        double Salary { get; set; }
        public Employee()
        {

        }
        public Employee(int id,string name,double salary)
        {
            this.ID = id;
            this.Name = name; 
            this.Salary = salary;
        }
        public override string ToString()
        {
            return $"员工号：{ID}     姓名:{Name}     薪水：￥{Salary}";
        }
    }
}
