﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Work2
{
//    问题描述：
//员工信息包括员工号（int），员工姓名和薪水，利用员工号作为键对象存储和检索员工信息。
//问题分析：
//首先定义一个员工类Employee,然后把员工号作为键对象把员工作为值存储在HashTable中，再根据用户输入的员工号取出对象的值对象。
//参考步骤：
//（1）	新建一个名称为HashTableTest的项目
//（2）	添加一个Employee类

    class Program
    {
        static void Main(string[] args)
        {
            HashTableTest();
            Console.ReadKey();
        }
        public static void HashTableTest()
        {
            Hashtable ht = new Hashtable();
            ht.Add(1,new Employee(1,"乐星星",3000));
            ht.Add(4, new Employee(4, "吴雪", 2000));
            ht.Add(3, new Employee(3, "吴刚", 1500));
            ht.Add(2, new Employee(2, "陈晶", 3500));
            //foreach (Employee e in ht.Values)
            //{
            //    Console.WriteLine(e);
            //}
            bool b = true;
            while (b)
            {
                Console.WriteLine("\r\n ======     请选择操作     =======");
                Console.WriteLine("  1.添加员工信息    2.查找      3.退出");
                Console.WriteLine(" =================================\r\n");
                Console.WriteLine("请输入你的选择：");
                string f=Console.ReadLine();
                switch(f)
                {
                    case "1":
                        Console.Write("请输入员工工号：");
                        int ID=int.Parse(Console.ReadLine());
                        if (ht.ContainsKey(ID))
                        {
                            Console.WriteLine("该员工已经存在！", "错误");
                            break;
                        }
                        Console.Write("请输入员工姓名：");
                        string Name=Console.ReadLine();
                        Console.Write("请输入员工薪水");
                        int Salary=int.Parse(Console.ReadLine());
                        ht.Add(ID, new Employee(ID, Name, Salary));
                        Console.WriteLine($"*******共有{ht.Count}位员工信息********");
                        break;
                    case "2":
                        Console.WriteLine("请输入你要查找的员工ID");
                        int IDFiled=int.Parse(Console.ReadLine());
                        object telFiled = ht[IDFiled];
                        if(telFiled==null)
                        {
                            Console.WriteLine("该员工不存在！", "错误");
                        }
                        else
                        {
                            Console.WriteLine($"你所查找的员工信息是：{telFiled}");
                        }
                        break;
                    case "3":
                        b = false;
                        break;
                    default:
                        Console.WriteLine("您输入是选项是错误的，请重新输入！","错误");
                        break;

                }
            }
        }
    }
}
