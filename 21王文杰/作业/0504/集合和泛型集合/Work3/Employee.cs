﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work3
{
    class Employee
    {
        int ID { get; set; }
        string Name { get; set; }
        int Salary { get; set; }
        public Employee()
        {

        }
        public Employee(int id, string name, int salary)
        {
            this.ID = id;
            this.Name = name;
            this.Salary = salary;
        }
        public override string ToString()
        {
            return $"员工号：{ID}    姓名:{Name}      薪水：￥{Salary}";
        }
    }
}
