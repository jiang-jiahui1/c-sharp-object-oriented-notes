## 类与对象深入

#### 		一.值类型与引用类型：

##### 			1.值类型：整型，浮点型，字符型，布尔型，货币型，枚举类型，结构

#####             2.引用类型：数组，string，接口，类

```C#
            int[] arr = new int[5];
            string a = "fhaklf";
            String a1 = new String("gag");
```

##### 			3.无参构造

##### 					形参：1.个数不同

```C#
		public Vehicle()
        {
            speed = 1000;
            
        }
        

        public Vehicle(double speed)
        {
            speed = 10;
            //不加this：代表是一个局部变量，就近原则
            //加this：全局变量
            //不能放在静态方法中
            Console.WriteLine("移动速度{0},{1}", this.speed,speed);
        }
```

#### 		二.this

##### 				1.不加this：代表是一个局部变量，就近原则

#####             	2.加this：全局变量

#####             	3.不能放在静态方法中

```C#
 		public Vehicle(double speed)
        {
            speed = 10;
            //不加this：代表是一个局部变量，就近原则
            //加this：全局变量
            //不能放在静态方法中
            Console.WriteLine("移动速度{0},{1}", this.speed,speed);
        }
```



#### 		三.值类型与引用类型区别

##### 				1.值类型在栈中开辟，自动释放，效率更高；引用类型在堆里面开辟，手动释放(GC垃圾回收)，

#####            	2.值类型不可以派生，没多态     引用类型可以派生，有多态



#### 		四、装箱拆箱(Object 名字)

##### 				1.装箱：将值类型转换为引用类型

```C#
			int st = 5;
            Object obj = st;
            Console.WriteLine("obj = {0}",obj);
```

##### 					2.拆箱：将引用类型转换为值类型

```C#
			 Object obj = st;
			int st1 = (int)obj;
            Console.WriteLine("st1 = {0}",st1);
```

#### 		五.函数返回值：在方法中将值返回：return,ref,out

##### 					1.return:返回

#####         			2.ref:引用传递：  改变实参的值， 使用前必须初始化

```C#
			int a = 5;
            int b = 8;
            Console.WriteLine("调用前a={0},b={1}", a, b);
            Swap(ref a,ref b);
            Console.WriteLine("引用传递a={0},b={1}",a,b);
		
		public  static  void Swap(ref int  a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
```



#####         			3.out:输出传递    返回多个值

```C#
			int c;
            int k ;
            int f ;
            Console.WriteLine($"执行之前，c{c},k{k},f{f}");
            swap1(out c,out k, out f);
            Console.WriteLine($"输出传递，c{c},k{k},f{f}");

		static void swap1(out int c, out int k, out int f)
        {
            c = 10;
            k = 7;
            f = 8;
        }
```

**例题：模拟登录：返回登陆是否成功(bool)，如果登录失败，提示用户是用户名错误还是密码错误**
          **admin 888888 两个返回值 一个bool，一个string**

```C#
using System;

namespace _220406._1
{
    class Program
    {
        static void Main(string[] args)
        {
            //模拟登录：返回登陆是否成功(bool)，如果登录失败，提示用户是用户名错误还是密码错误
            //admin 888888 两个返回值 一个bool，一个string
            bool b = true;
            string c = null;
            B1(out b,out c);
            if (b == false)
            {
                Console.WriteLine(c);
            }
        }
        public static void B1(out bool b,out  string c)
        {
            b = true;
            c = null;
            Console.WriteLine("请输入账号");
            string n = Console.ReadLine();
            Console.WriteLine("请输入密码");
            string a = Console.ReadLine();
            if(a != "888888")
            {
                b = false;
                c = "登录失败，密码错误";
                return;
            }
            if(n!= "admin")
            {
                b = false;
                c = "登录失败，账号错误";
                return;
            }
        }
    }
}

```

