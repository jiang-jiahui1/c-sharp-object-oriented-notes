## 继承_中

#### 	1.构造方法的调用顺序

##### 		先调用基类中的无参构造方法,再调用子类中对应的构造方法

```C#
using System;
using System.Collections.Generic;
using System.Text;

namespace TEST12继承_中
{
    class Animal
    {
        protected string name;
        protected int age;
        protected string gender;

        public Animal()
        {
            Console.WriteLine("基类Animal的无参构造方法");
            age = 1;
            gender = "雌性";

        }

        public Animal(string name,int age, string gender)
        {
            Console.WriteLine("调用基类Animal中的带参构造方法");
            this.name = name;
            this.age = age;
            this.gender = gender;
        }

        
        
    }

    class Fish:Animal
    {
        string _character;
        //public Fish()
        //{
        //    Console.WriteLine("子类Fish的无参构造方法");
        //    name = "鱼";

        //    gender = "雌性";
        //}
        public Fish(string name,int age, string gender, string chare):base(name,age,gender)
        {
            Console.WriteLine("子类Fish的带参构造方法");
            this._character = chare;
            //this.age = age;
            //this.name = "鱼";
        }
        public void Intro()
        {
            Console.WriteLine("种类  {0}\n性别  {1}\n年龄  {2}特性{3}", name, gender, age, _character);
        }
    }
}

```



#### 	2.子类中调用基类指定的构造方法：base

##### 		使用base指定调用

```C#
using System;
using System.Collections.Generic;
using System.Text;

namespace TEST12继承_中
{
    class Animal
    {
        protected string name;
        protected int age;
        protected string gender;

        public Animal()
        {
            Console.WriteLine("基类Animal的无参构造方法");
            age = 1;
            gender = "雌性";

        }

        public Animal(string name,int age, string gender)
        {
            Console.WriteLine("调用基类Animal中的带参构造方法");
            this.name = name;
            this.age = age;
            this.gender = gender;
        }

        
        
    }

    class Fish:Animal
    {
        string _character;
        //public Fish()
        //{
        //    Console.WriteLine("子类Fish的无参构造方法");
        //    name = "鱼";

        //    gender = "雌性";
        //}
        public Fish(string name,int age, string gender, string chare):base(name,age,gender)
        {
            Console.WriteLine("子类Fish的带参构造方法");
            this._character = chare;
            //this.age = age;
            //this.name = "鱼";
        }
        public void Intro()
        {
            Console.WriteLine("种类  {0}\n性别  {1}\n年龄  {2}特性{3}", name, gender, age, _character);
        }
    }
}

```



#### 	3.当前类调用另一个构造方法：this

```C#
using System;
using System.Collections.Generic;
using System.Text;

namespace TEST12继承_中
{
    sealed class Plants
    {
        string _name;
        int _age;
        string _type;

        public Plants(string name, int age)
        {
            this._name = name;
            this._age = age;
        }

        public Plants(string type,string name,int age):this(name,age)
        {
            this._type = type;
        }

        public void Intro()
        {
            Console.WriteLine("名字{0}\n种类{1}\n年龄{2}",_name,_type,_age);
        }
    }

    class AppleTree
    {

    }
}

```



#### 	4.不能被继承的类：密封类(sealed)