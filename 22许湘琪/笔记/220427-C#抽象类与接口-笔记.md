## 抽象类与接口

#### 	一.抽象类(abstract)

##### 		1.抽象类中：可以包含非抽象成员和抽象成员(方法，属性，索引器，事件)

##### 		2.抽象类不能进行实例化

```C#


namespace 作业
{
    class Program
    {
        static void Main(string[] args)
        {
            //实例化：父类的对象 指向子类的一个实例
            Shape s = new Circle() { Radius = 5 };
            
            s.GetArea();

        }
    }
    abstract class Shape
    {
        //抽象类中：可以包含非抽象成员和抽象成员(方法，属性，索引器，事件)
        //抽象类不能进行实例化

        public abstract void GetArea();    //抽象方法

        public abstract double Radius { get; set; }  //抽象属性
    }


    //子类必须全部具体实现父类中的抽象成员
    class Circle : Shape
    {
        double _radius;

        public override double Radius 
        { 
            get { return _radius; } 
            set { this._radius = value; } 
        }

        public override void GetArea()
        {
            Console.WriteLine("圆的面积{0}",_radius);
        }
       
    }
}

```

#### 	二.接口（interface）：更加纯粹的抽象类

##### 			1.C#中的接口以大写字母“I”开头

##### 			2.接口中只能声明非静态的：方法、属性、索引器和事件

##### 			3.接口不能声明字段、构造方法、常量和委托

##### 			4.接口的成员默认是public的

##### 			5.接口中所有的方法、属性和索引器都必须没有实现

##### 			6.接口的对象指向一个具体类的实例化

```C#
using System;

namespace TEST14抽象类与接口
{
    class Program
    {
        static void OutPut(ITest n)   //测试方法
        {
            Console.WriteLine("姓名:{0},年龄{1}",n.GetName(),n.GetAge());
        }
        static void Main(string[] args)
        {
            //值类型：整型，浮点型，布尔型，字符型(char),结构，枚举
            //引用类型：类，数组，集合，接口，字符串
            //TestA a = new TestA() { name = "王文杰", age = 99};
            //TestB b = new TestB() { firstName = "王", lastName = "文杰", age = 99.000000};

            //接口 = 具体类的实例
            //接口a，由TestA实现


            //接口的对象指向一个具体类的实例化。
            ITest a = new TestA() { name = "王文杰", age = 99 };
            //接口b，由TestB实现
            Console.WriteLine("姓名：{0}，年龄{1}",a.GetName(),a.GetAge());
            ITest b = new TestB() { firstName = "王", lastName = "文杰", age = 99.000000 };
            //OutPut(a);
            Console.WriteLine("-------------------");
            OutPut(b);

            
        }
    }
}

namespace TEST14抽象类与接口
{
    interface ITest   //更加纯粹的抽象类
    {
        
        //接口体：public， abstract   不要写出来   方法，属性，事件，索引器
        string GetName();  //方法
        string GetAge();
    }

    class TestA:ITest
    {
        public string name;
        public int age;
        public string GetName()
        {
            return name;
        }

        public string GetAge()
        {
            return age.ToString();
        }
    }

    class TestB:ITest
    {
        public string firstName;
        public string lastName;
        public double age;

        public string GetName()
        {
            return firstName + lastName;
        }

        public string GetAge()
        {
            return age.ToString();
        }
    }

}

```

