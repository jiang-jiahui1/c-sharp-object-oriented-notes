﻿using System;

namespace _3._31_方法
{
    class work1
    {
        static void main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //1.声明4个函数，分别用来计算两个数的和、差、积、商。

            //输入数字
            Console.WriteLine("请输入第一个数");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入第二个数");
            int b = Convert.ToInt32(Console.ReadLine());

            //引用方法
            sum(a, b);
            cha(a, b);
            ji(a, b);
            shang(a, b);

        }
        static  void sum(int a,int b)
        {
            int sum = 0;
            sum = a + b;
            
            Console.WriteLine("两个数的和为"+sum);

        }
        static void cha(int a, int b)
        {
          
            int cha = a - b;

            Console.WriteLine("两个数的差为" + cha);

        }
        static void shang(int a, int b)
        {
            int shang= a / b;

            Console.WriteLine("两个数的商为" +shang);

        }
        static void ji(int a, int b)
        {
            int ji= a * b;

            Console.WriteLine("两个数的积为" + ji);

        }
    }
}
