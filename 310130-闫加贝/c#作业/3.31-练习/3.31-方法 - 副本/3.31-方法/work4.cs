﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3._31_方法
{
    class work4
    {
        static void Main(string[] args)
        {
            //5.定义一函数，用于求2个数中的较大数，并将其返回，这2个数字在主函数中由用户输入
            Console.WriteLine("请输入第一个数");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入第二个数");
            int b = Convert.ToInt32(Console.ReadLine());
            max(a, b); 
            

        }
        static void max(int a, int b)
        {
            int c = a < b ? b : a;
            Console.WriteLine("最大的数为" + c);

        }
    }
}
