﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3._31_方法
{
    class work3
    {
        static void main(string[] args)
        {
            //4.根据用户不同的选择计算不同形状的面积（三角形、正方形、长方形）
            //（用不同方式实现：1.参数和面积在自定义函数中输入和输出 
            //    2.参数在主函数中接收 
            //    3.参数在主函数中接收并且在主函数中要计算面积和）  
            Console.WriteLine("请输入要求图形的形状");
            string name = Convert.ToString(Console.ReadLine());
            Console.WriteLine("请输入长");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入宽");
            int b = Convert.ToInt32(Console.ReadLine());
            if (name == "三角形")
            {
                sj(a,b);
            }
            if (name == "正方形")
            {
                zf(a);
            }
            if (name == "长方形")
            {
                cf(a, b);
            }

        }
        static void sj(int a,int b)
        {
            int c = 1;
            c = a * b / 2;
            Console.WriteLine("三角形的面积为"+c);
        }
        static void zf(int a)
        {
            int c = 1;
            c = a * a ;
            Console.WriteLine("正方形的面积为" + c);
        }
        static void cf(int a, int b)
        {
            int c = 1;
            c = a * b ;
            Console.WriteLine("长方形的面积为" + c);
        }
    }
}
