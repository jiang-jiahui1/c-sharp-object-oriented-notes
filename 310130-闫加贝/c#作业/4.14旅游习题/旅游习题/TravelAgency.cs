﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 旅游习题
{
    class TravelAgency
    {
        string _place;
        decimal _cost;
        public TravelAgency(string place,decimal cost)
        {
            this._place = place;
            this._cost = cost;
        }
        public string Tplace
        {
            get
            {
                return _place;
            }
        }
        public decimal Cost
        {
            get
            {
                return _cost;
            }
        }


    }
}
