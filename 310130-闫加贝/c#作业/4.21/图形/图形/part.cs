﻿using System;
using System.Collections.Generic;
using System.Text;


//1.图形类：
//  求周长，面积（三角形， 四边形， 圆形）


namespace 图形
{
    class part
    {
       protected double 周长;
        protected double 面积;
       public string name;
        protected string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        public void 输出()
        {
            Console.WriteLine("名字:{0},周长:{1},面积{2}",name,周长,面积);
        }
    }
    class 三角形: part
    {
        public double a;
        public double b;
        public double d;
        public void z()
        {
            周长= a + b + d;
            double h = 周长 / 2; 
            面积 = Math.Sqrt(h * (h - a) * (h - b) * (h - d));
        }

    }

    class 四边形 : part
    {
        public double length;
        public double width;
        public void z()
        {
            周长 = (length + width) * 2;
            面积 = length * width;
        }
        
    }

    class 圆 : part
    {
        public double 半径;
        public void z()
        {
            周长 = 2 * 3.14 * 半径;
            面积 = 3.14 * 半径 * 半径;
        }
    }
}
