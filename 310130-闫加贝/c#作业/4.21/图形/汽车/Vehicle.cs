﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 汽车
{
//    3.设计一个汽车类Vehicle，包含的属性有车轮个数wheels和车重weight。
//小车类Car是Vehicle的子类，其中包含的属性有载人数loader。
//卡车类Truck是Car类的子类，其中包含的属性有载重量payload。
//每个类都有构造方法和输出相关数据的方法。
    class Vehicle
    {
        public string name;
        public string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        protected int wheels;
        public int Wheels
        {
            get { return wheels; }
            set { this.wheels = value; }
        }
        protected double weight;
        public double Weight
        {
            get { return weight; }
            set { this.weight = value; }
        }
        public void and()
        {
            Console.WriteLine("车型:{0},轮胎数:{1},车重{2}",name,wheels,weight);
        }

    }
    class car : Vehicle
    {
        public int _loader;
        public int loader
        {
            get { return _loader; }
            set { this._loader = value; }
        }
        public void Intro()
        {
            name = "奔驰";
            Console.WriteLine("汽车类型:{0}\n车轮数:{2}\n核载人数:{1},", name, _loader, wheels);
        }
    }

    class Truck : Vehicle
    {
        public double _payload;
        public double payload
        {
            get { return _payload; }
            set { this._payload = value; }
        }
        public void Intro()
        {
            name = "霸道";
            Console.WriteLine("汽车类型:{0}\n车轮数:{2}\n核载重量:{1},", name,_payload, wheels);


        }


    }
}
