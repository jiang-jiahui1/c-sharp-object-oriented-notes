﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4._25
{
    //雇员系统，定义雇员基类，共同的属性，姓名、地址和出生日期(可有可无)，
    //子类：程序员，秘书，高层管理，清洁工，
    //他们有不同的工资算法，其中高级主管和程序员采用底薪加提成(6000)的方式，
    //高级主管和程序员的底薪分别是5000元和2000元?，秘书和清洁工采用工资的方式，
    //工资分别是3000和1000，（以多态的方式处理程序。）
    class employee
    {
        public string _name;
        public string _dress;
        public decimal _base;
        public string _date;

        public employee(string name,string dress, string date,decimal bases)
        {
            this._base = bases;
            this._date = date;
            this._dress = dress;
            this._name = name;
            

        }
        public string Name
        {
            get { return _name; }
            set { this._name = value; }
        }
        public string Dress
        {
            get { return _dress; }
            set { this._dress = value; }
        }
        public string Date
        {
            get { return _date; }
            set { this._date = value; }
        }
        public decimal Base
        {
            get { return _base; }
            set { this._base = value; }
        }
        public virtual void say()
        {
            Console.WriteLine("工资为:");
        }
    }
    class CH : employee
    {
       protected decimal Ti;
        public CH(decimal ti, string name, string date, string dress, decimal bases) : base(name, dress,date,bases)
        {
            this.Ti = ti;
        }
        public override void say()
        {
            Console.WriteLine("工种:{0}",_name);
            Console.WriteLine("出生日期:{0}",_date);
            Console.WriteLine("住址:{0}",_dress);
            Console.WriteLine("工资:{0}",_base+Ti);
            Console.WriteLine();
        }
    }
    class GG : employee
    {
        protected decimal Ti;
        public GG(decimal ti, string name, string date, string dress, decimal bases) : base(name, dress, date, bases)
        {
            this.Ti = ti;
        }
        public override void say()
        {
            Console.WriteLine("工种:{0}", _name);
            Console.WriteLine("出生日期:{0}", _date);
            Console.WriteLine("住址:{0}", _dress);
            Console.WriteLine("工资:{0}", _base + Ti);
            Console.WriteLine();
        }

    }
    class MS : employee
    {
        public MS( string name, string date, string dress, decimal bases) : base(name, dress, date, bases)
        {
            
        }
        public override void say()
        {
            Console.WriteLine("工种:{0}", _name);
            Console.WriteLine("出生日期:{0}", _date);
            Console.WriteLine("住址:{0}", _dress);
            Console.WriteLine("工资:{0}", _base);
            Console.WriteLine();
        }
    }
    class QJ : employee
    {
        public QJ(string name, string date, string dress, decimal bases) : base(name, dress, date, bases)
        {

        }
        public override void say()
        {
            Console.WriteLine("工种:{0}", _name);
            Console.WriteLine("出生日期:{0}", _date);
            Console.WriteLine("住址:{0}", _dress);
            Console.WriteLine("工资:{0}", _base);
            Console.WriteLine();
        }
    }
}
