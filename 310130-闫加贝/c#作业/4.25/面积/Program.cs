﻿using System;

namespace 面积
{
//    创建一个Shape类,此类包含一个名为color的数据成员(用于存放颜色值)和一个
//GetColor方法(用于获取颜色值)，这个类还包含一个名为GetArea的虚方法。
//用这个类创建名为Circle和Square的两个子类，这两个类都包含两个数据成员，
//即radius和sideLen。这些派生类应提供GetArea方法的实现，以计算相应形状的
//面积。

    class Program
    {
        static void Main(string[] args)
        {
            Shape a = new Circle(5, "圆");
            a.GetArea();
            Shape b = new Square(6, "正方形");
            b.GetArea();

        }
    }
}
