﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 面积
{
    //    创建一个Shape类,此类包含一个名为color的数据成员(用于存放颜色值)和一个
    //GetColor方法(用于获取颜色值)，这个类还包含一个名为GetArea的虚方法。
    //用这个类创建名为Circle和Square的两个子类，这两个类都包含两个数据成员，
    //即radius --半径  和sideLen --边长。这些派生类应提供GetArea方法的实现，以计算相应形状的
    //面积。
    class Shape
    {
        public string _color; //(用于存放颜色值)
        public string GetColor //GetColor方法(用于获取颜色值)
        {
            get { return _color; }
            set { this._color = value; }
        }
        public Shape(string color)
        {
            this._color = color;
        }
        public virtual void GetArea()   //名为GetArea的虚方法
        { 
           
        }
    }
    class Circle : Shape
    {
        protected double _radius;
        public Circle(double radius,string color) : base(color)
        {
            this._radius = radius;
        }
        public override void GetArea()
        {
            Console.WriteLine("颜色值:{0}",_color);
            Console.WriteLine("面积:{0}",3.14*_radius*_radius);
            Console.WriteLine();
        }


    }
    class Square : Shape
    {
       protected double _sideLen;
        public Square(double sidelen,string color) : base(color)
        {
            this._sideLen = sidelen;
        }
        public override void GetArea()
        {
            Console.WriteLine("颜色值:{0}", _color);
            Console.WriteLine("面积:{0}", _sideLen* _sideLen);
            Console.WriteLine();
        }
    }

}
