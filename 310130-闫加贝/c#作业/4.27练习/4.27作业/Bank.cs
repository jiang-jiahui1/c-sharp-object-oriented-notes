﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4._27作业
{
//         创建一个银行接口，有存钱，取钱方法，还有账号余额属性，
    //然后实现这个接口。
    //     定义一个Account类，在里面实现具体的存钱，取钱方法。并且实现余额的读取
    //属性
    interface IBank
    {
        decimal Mind { get; set; }
        decimal First(decimal first); 
        decimal Get(decimal save);
        decimal Give(decimal give);
        void yue();
        
    }
    class Account : IBank
    {
        decimal mind=0;

        public decimal Mind { get => mind; set => mind = value; }

        public  decimal First(decimal first)
        {
            Mind = first;
            return first;
        }
        public decimal Get(decimal save)
        {
            Mind += save;
            return save;
        }
        
        public decimal Give(decimal give)
        {
            Mind -= give;
            return give;
        }
        
        public void yue()
        {
            Console.WriteLine("余额{0}",Mind);
        }


    }
}
