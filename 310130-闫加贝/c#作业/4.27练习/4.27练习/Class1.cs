﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4._27练习
{
//    2，	按要求编写C#程序：
//（1）编写一个接口：InterfaceA，只含有一个方法int Method(int n)；
//（2）编写一个类：ClassA来实现接口InterfaceA，实现int Method(int n)接口方法时，要求计算1到n的和；
//（3）编写另一个类：ClassB来实现接口InterfaceA，实现int Method(int n)接口方法时，要求计算n的阶乘（n!）；
//（4）编写main方法中使用接口回调的形式来测试实现接口的类。
interface IInterfaceA
    {
        int Method(int n);
    }
    class ClassA:IInterfaceA
    {
       
        public  int Method(int n)
        {
            int acc=0;
            for (int i = 1; i <= n; i++)
            {
                acc += i;
            }
            return acc;
        }


    }
    class ClassB : IInterfaceA
    {

        public int Method(int n)
        {
            int ji =1;
            for (int i = 1; i < n; i++)
            {
                ji *= i;
            }
            return ji;
        }


    }
}
