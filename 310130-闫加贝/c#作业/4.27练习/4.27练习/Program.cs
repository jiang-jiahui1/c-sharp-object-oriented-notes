﻿using System;

namespace _4._27练习
{

//    2，	按要求编写C#程序：
//（1）编写一个接口：InterfaceA，只含有一个方法int Method(int n)；
//（2）编写一个类：ClassA来实现接口InterfaceA，实现int Method(int n)接口方法时，要求计算1到n的和；
//（3）编写另一个类：ClassB来实现接口InterfaceA，实现int Method(int n)接口方法时，要求计算n的阶乘（n!）；
//（4）编写main方法中使用接口回调的形式来测试实现接口的类。

    class Program
    {
        static void Main(string[] args)
        {
            IInterfaceA i1 = new ClassA();
            i1.Method(5);
            Console.WriteLine("累加为:{0}",i1.Method(5));
            IInterfaceA i2 = new ClassB();
            i2.Method(5);
            Console.WriteLine("阶乘为:{0}",i2.Method(5));


        }
    }
}
