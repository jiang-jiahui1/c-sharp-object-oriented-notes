﻿using System;

namespace _4._28.练习二
{
//    	定义一个接口，接口名为ICard，其中定义一个成员方法，方法名自定义，方法要实现的功能是对电话卡进行扣款。
//定义两个子类，实现ICard接口，实现其中的扣款方法，一个类实现201卡(前3分钟0.2元，之后每分钟0.1元)的扣款功能，一个类实现校园卡（每分钟0.1元）的扣款功能，每个类有一个成员变量banlance保存余额，默认为100。

    class Program
    {
        static void Main(string[] args)
        {
            ICard i1 = new Tcard();
            i1.Balance = 100;
            Console.WriteLine("201卡:",i1.Minute=100000);
            i1.kou();

            ICard i2 = new Scard();
            i2.Balance = 100;
            Console.WriteLine("校园卡:",i2.Minute=5);
            i2.kou();
        }
    }
}
