﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4._28HomeWork
{


    //一、	编写一个程序以实现家用电器的层次结构，此层次结构将包含电器ElectricEquipment抽象类和空调类AirCondition。ElectricEquipment类应包含一个表示电器工作的Working()方法，该方法应该在子类中被事项。
    abstract class ElectricEquipment
    {
        protected int Gl;
        protected ElectricEquipment(int gl)
        {
            Gl = gl;
        }
        public abstract void Working();

    }
    class AirCondition : ElectricEquipment
    {
        public AirCondition(int a) : base(a)
        {

        }
        public override void Working()
        {
            Console.WriteLine("空调功率在"+this.Gl+"下可以制冷也可以加热");
        }
    }
}
