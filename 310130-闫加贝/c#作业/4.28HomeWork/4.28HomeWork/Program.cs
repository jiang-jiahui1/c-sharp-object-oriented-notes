﻿using System;

namespace _4._28HomeWork
{
  
    //一、	编写一个程序以实现家用电器的层次结构，此层次结构将包含电器ElectricEquipment抽象类和空调类AirCondition。ElectricEquipment类应包含一个表示电器工作的Working()方法，该方法应该在子类中被事项。
    
    class Program
    {
        static void Main(string[] args)
        {
            AirCondition ar = new AirCondition(1500);
            ar.Working();
            Console.ReadKey();
        }
    }
}
