﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 练习2
{
    //1、定义一个Account类
    class Account
    {
        //2、声明成员变量balance表示余额
        public decimal balance;
//3、编写一个无参的构造方法，设置默认余额为1000 

        public Account(decimal Bal)
        {
            this.balance = Bal;
        }
        //5、编写三个成员方法，分别表示存钱，取钱和查看余额
        //存钱
        public void saveBal(decimal sBal)
        {
            Console.WriteLine("您已存款{0}元。", sBal);
            balance += sBal;
        }
        //取钱
        public void outBal(decimal oBal)
        {
            Console.WriteLine("取款{0}元成功", oBal);
            balance -= oBal;

        }
        //查看余额
        public void checkBal()
        {
            Console.WriteLine("余额{0}元。", balance);
        }


        



    }
}
