﻿using System;

namespace 练习2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //4、编写一个有参的构造方法，让客户指定余额  设置默认余额为1000
            Account ac = new Account(1000);
            ac.saveBal(5000M);
            ac.outBal(2000M);
            ac.checkBal();
        }
    }
}
