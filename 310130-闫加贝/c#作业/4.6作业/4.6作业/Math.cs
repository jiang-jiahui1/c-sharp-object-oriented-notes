﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4._6作业
{
    //二、	创建一个Math类，里边定义两个静态的方法，
    //一个用于求两个数的和，另一个用于求三个数的和，
//方法名都定义成Add()（利用方法的重载）
    class Math
    {
       public static void Add(int a,int b)
        { int add = a + b;
            Console.WriteLine("和为{0}", add);
        }
       public static void Add(int a, int b,int c)
        {
            int add = a + b+c;
            Console.WriteLine("和为{0}", add);
        }
    }
}
