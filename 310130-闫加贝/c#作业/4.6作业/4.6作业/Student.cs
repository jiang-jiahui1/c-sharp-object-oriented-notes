﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4._6作业
{

    //一、	创建一个Student类，类中包含三个公共成员变量：stuName(学生姓名)，stuAge(学生年龄)，stuSex(学生性别)，
    //一个静态成员变量tcAge(共青团员退团年龄)，定义一个构造函数用于初始化学生姓名，学生年龄，学生性别，
    //再定义一个无参构造函数，再定义一个静态的方法SayHello()，输出学生的所有信息，实例化一个对象Mary
    class Student
    {
        public string stuName;
        public int stuAge;
        public string stuSex;
        public static int tcAge;

        public Student(string StuName, int StuAge, string StuSex,int tcAge)
        {
            this.stuAge = StuAge;
            this.stuName = StuName;
            this.stuSex = StuSex;
            Student.tcAge = tcAge;
        }
        //public static int tcAge( int a)
        //{
        //    tcAge = a;

        //}
        //public void sn( string stuName)
        //{


        //}
        //public void ss(string stuSex)
        //{


        //}
        //public void sa(string stuAge)
        //{


        //}
        public static void SayHello(string stuName ,string stuSex ,int stuAge, int tcAge)
        {
            Console.WriteLine("学生姓名:{0}",stuName);
             
            Console.WriteLine("学生性别:{0}",stuSex);
             
            Console.WriteLine("学生年龄:{0}",stuAge);
             
            Console.WriteLine("退团年龄:{0}",tcAge );
            
            Console.WriteLine("{0},{1},{2},{3}", stuName, stuAge, stuSex, tcAge);
        }
    }
}
