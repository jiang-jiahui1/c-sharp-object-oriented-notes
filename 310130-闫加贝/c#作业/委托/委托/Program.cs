﻿using System;

namespace 委托
{
    class Program
    {
        delegate void myDelegate(int[] n);
        static void Main(string[] args)
        {
            int[] n = { 1, 6, 9 };
            myDelegate md = delegate (int[] n)
            {
                for (int i = 0; i < n.Length; i++)
                {
                    n[i] = n[i] * n[i];
                }
            };
            md(n);
            foreach (var i in n)
            {
                Console.WriteLine(i);
            }

        }
    }
}
