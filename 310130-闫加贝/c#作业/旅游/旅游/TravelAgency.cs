﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 旅游
{
    class TravelAgency
    {
        string _place;
        decimal _cost;
        public TravelAgency(string place, decimal cost)
        {
            this._place = place;
            this._cost = cost;
        }

        public string Place
        {
            get
            {
                return _place;
            }
        }


        public decimal Cost
        {
            get
            {
                return _cost;
            }
        }

    }
}
