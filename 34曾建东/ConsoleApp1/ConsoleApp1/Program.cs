﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //题目1
            //ti1();

            //题目2
            //ti2();

            //题目3
            //ti3();

            //题目4
            //ti4();

            //题目5
            //ti5();

            //题目6
            //ti6();

            //题目7
            ti7();
        }
        static void ti7() {
            //1.求出100 - 200之间的所有素数，并按照每行5个输出.
            int num = 0;
            int cc = 0;
            for (int i = 100; i <= 200; i++) {
                for (int j = 1; j < i; j++) {
                    if (i % j == 0) {
                        num++;
                    }
                }
                if (num == 1) {
                    Console.Write("{0}\t", i);
                    cc++;
                    if (cc % 5 == 0) {
                        Console.WriteLine();
                    }
                }
                num = 0;
            }



        }
        //题6
        static void ti6() {
            //判断一个数是不是水仙花数
            Console.WriteLine("请输入您要验证的数");
            int num = Convert.ToInt32(Console.ReadLine());
            if ((num + "").Length != 3)
            {
                Console.WriteLine("您输入的不是水仙花数");
            }
            else {
                int ge = num % 10;
                int sh = num /10% 10;
                int ba = num /10/10% 10;
                if (ge* ge * ge + sh* sh * sh +ba* ba * ba  == num)
                {
                    Console.WriteLine("该数是水仙花数");
                }
                else {
                    Console.WriteLine("您输入的不是水仙花数");
                }
            }

        }
        //题5
        static void ti5() {
            //输入3个数,按大小输出这3个数（例如：用户输入的是：10  5  20  ，排序后为：5     10   20）
            int[] arr = new int[3];
            for (int i = 0; i < arr.Length; i++) {
                Console.WriteLine("请输入第{0}个数值", i+1);
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("排序前：");
            for (int k = 0; k < arr.Length; k++)
            {
                Console.Write(arr[k]+"\t");
                if (k == arr.Length - 1) {
                    Console.WriteLine();
                }
            }
            for (int i = 0; i < arr.Length; i++)
            {
                for (int k = 0; k < arr.Length-1; k++)
                {
                    if (arr[k + 1] > arr[k]) {
                        int num = arr[k];
                        arr[k] = arr[k + 1];
                        arr[k + 1] = num;
                    }
                }
            }
            Console.WriteLine("排序后:");
            for (int k = 0; k < arr.Length; k++)
            {
                Console.Write(arr[k] + "\t");
            }
        }
        //题4
        static void ti4() {
            //接受用户输入的四个数字a,b,c,d，
            //将四个数中的最大值求出来（例如输出结果为：a的值为20，a最大）！
            Console.WriteLine("请输入a的值");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入b的值");
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入c的值");
            int c = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入d的值");
            int d = Convert.ToInt32(Console.ReadLine());
            int max = a > (b > (c > d ? c : d) ? b : (c > d ? c : d)) ? a : (b > (c > d ? c : d) ? b : (c > d ? c : d));
            if (a == max) {
                Console.WriteLine("a的值为：{0}\r\n最大", max);
            }
            else if (b == max) {
                Console.WriteLine("b的值为：{0}\r\n最大", max);
            }
            else if (c == max)
            {
                Console.WriteLine("c的值为：{0}\r\n最大", max);
            }
            else if (d == max)
            {
                Console.WriteLine("d的值为：{0}\r\n最大", max);
            }


        }

        //题3
        static void ti3() {
            //某超市进行促销活动，规则如下：
            //    如果是本超市的会员，
            //    购买的商品总价在100元以上，八折优惠；
            //    如果是会员但商品总价在100元以下9折优惠；
            //    如果非会员商品总价在100以上九折优惠；
            //    如果非会员商品总价在100以下，不打折；询问用户是否是会员，和商品的总价，
            //    根据答案判断折扣额以及最终要付款的金额
            Console.WriteLine("请问您是否有本超市会员");
            if (Console.ReadLine() == "是")
            {
                Console.WriteLine("请输入您本次消费的金额");
                if (Convert.ToInt32(Console.ReadLine()) >= 100)
                {
                    Console.WriteLine("您可享受八折优惠");
                }
                else {
                    Console.WriteLine("您可享受九折优惠");
                }
            }
            else {
                Console.WriteLine("请输入您本次消费的金额");
                if (Convert.ToInt32(Console.ReadLine()) >= 100)
                {
                    Console.WriteLine("您可享受九折优惠");
                }
                else
                {
                    Console.WriteLine("您暂无可享受的优惠");
                }
            }




        }
        //题2
        static void ti2(){
            //1.某公司即将为员工买保险，要求如下：
            //    已婚的男、女性都可以买保险，
            //    未婚男性25岁以下不提供保险，
            //    25岁以上（包括）提供保险，未婚女性22岁以下不提供保险，
            //    22岁以上（包括）提供保险，
            //    请用程序实现以上功能，接受员工的信息，
            //    显示公司是否为他提供保险，
            //    并显示原因（如：因为已婚，公司为你提供保险；公司不能为你提供保险，因为未婚男性在25岁以上才能提供保险）

            Console.WriteLine("请输入您是否已婚");
            if (Console.ReadLine() == "是")
            {
                Console.WriteLine("因为已婚，公司为你提供保险");
            }
            else {
                Console.WriteLine("请输入您的性别");
                if (Console.ReadLine() == "男") {
                    Console.WriteLine("请输入您的年龄");
                    int age = Convert.ToInt32(Console.ReadLine());
                    if (age < 25)
                    {
                        Console.WriteLine("25岁以下男性不提供保险");
                    }
                    else {
                        Console.WriteLine("年龄到达或超过25岁男性提供保险");
                    }
                } 
                else{
                    Console.WriteLine("请输入您的年龄");
                    int age = Convert.ToInt32(Console.ReadLine());
                    if (age < 22)
                    {
                        Console.WriteLine("22岁以下女性不提供保险");
                    }
                    else
                    {
                        Console.WriteLine("年龄到达或超过22岁女性提供保险");
                    }
                }
            }






        }
        //题1
        static void ti1() {

            //            1.先声明一个存储整数的变量a(年龄)，然后将a赋值20,最后输出a;
            int a = 10;
            Console.WriteLine("年龄：{0}", a);

            //            2.声明并同时初始化变量b(分数)，值为80.5,最后输出b;
            double b = 80.5;
            Console.WriteLine("分数：{0}", b);
            //            3.声明并同时初始化字符串m(名字),值为自己的名字,并输出。
            string m = "myname";
            Console.WriteLine("姓名：{0}", m);
            //            4.  * *(挑战)利用转义字符按照格式输出
            Console.WriteLine("姓名\t年龄\t分数\r\n{0}\t{1}\t{2}", m, a, b);
        }

    }
}
