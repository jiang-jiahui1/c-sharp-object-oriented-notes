          多播委托
+ 和+=可以合并委托实例
-和-=可以移除委托实例
      myDelegate md =  方法1;
      md += 方法2  // md = md + 方法2（可以是null）

调用md时就会调用方法1和方法2
并且调用顺序与他们的定义顺序一致

事件使用步骤：
（定义委托）
[访问修饰符] event  委托名  事件名 ;      (定义事件)
事件名 += new 委托(事件处理方法名); （订阅事件）
事件名（[参数列表]);                           （引发事件）
示例：
public delegate void dellisentner();  //1.定义委托
class Teachear
    {
        public event dellisentner lisentnerevent;    //2.定义事件

        public void beginclass()
        {
            Console.WriteLine("老师宣布，现在开始上课了！");
            if (lisentnerevent != null)
            {
                lisentnerevent();
            }
        }
    }

Program定义静态方法:  getValue()，用于获取
       //数组arr{1,3,5}的最大值
匿名方法：Square(int n)
    //用于返回n的平方值。

返回的是最后一个方法的值，前面的返回值就弃用。
            myDel md1 = p.getMax;
            md1 += p.getMin;
