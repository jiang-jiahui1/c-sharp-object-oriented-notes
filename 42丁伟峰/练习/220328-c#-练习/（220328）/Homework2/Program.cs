﻿using System;

namespace Homework2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. 某公司即将为员工买保险，要求如下：已婚的男、女性都可以买保险，未婚男性25岁以下不提供保险，25岁以上（包括）提供保险，未婚女性22岁以下不提供保险，22岁以上（包括）提供保险，请用程序实现以上功能，接受员工的信息，显示公司是否为他提供保险，并显示原因（如：因为已婚，公司为你提供保险；公司不能为你提供保险，因为未婚男性在25岁以上才能提供保险）
            Console.WriteLine("请你输入性别");
            string i = Console.ReadLine();
            Console.WriteLine("请输入你的年龄");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("是否已婚");
            string m = Console.ReadLine();
            if (m == "是")
            {
                Console.WriteLine("因为已婚，公司为你提供保险");
            }
            if (m=="否") {
                if (i == "男" && a > 25 || i == "女" && a > 22)
                {
                    Console.WriteLine("公司为你提供保险");
                }
                else { Console.WriteLine("公司不能为你提供保险，因为未婚男性在25岁以上才能提供保险，未婚女性在22岁以上能提供保险"); }
                }
        }
    }
}
