﻿using System;

namespace 热水器
{
    class Jiarenqi
    {
        public delegate void Muyu();

        public event Muyu Shuiwenggao;

        public void gao() 
        {
            Console.WriteLine("开始加热，现在水温为10~C");
            if (Shuiwenggao!=null)
            {
                Shuiwenggao();
            }
        }
    }

    class Baojingqi
    {
        int _name;

        public Baojingqi(int name)
        {
            this._name = name;
        }
        public void baojing()
        {
            Console.WriteLine("警报器：现在的水温为{0}~C",_name) ;
        }
    }
    class Xianshiqi
    {
        int _name;
        public Xianshiqi(int name) 
        {
            this._name = name;
        }
        public void xianshi()
        {
            if (_name>=95)
            {  
                Console.WriteLine("显示器：{0}~C",_name);
            }
        }
    }
    class Program 
    {
        static void Main(string[] args) 
        {
            Baojingqi s1 = new Baojingqi(95);
            Xianshiqi s2 = new Xianshiqi(95);

            Jiarenqi wa = new Jiarenqi();
            wa.Shuiwenggao += s2.xianshi;
            wa.Shuiwenggao += s1.baojing;
            wa.gao();
        }
    }
}
