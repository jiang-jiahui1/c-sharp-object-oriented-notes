﻿using System;

namespace ConsoleApp2
{
    //为旅行社编写一个程序，用于接受用户输入的旅游地点。
    //接下来验证旅行社是否能满足用户对地点的请求，验证之后显示相应消息，
    //给出该旅游套餐的费用。注意：定义一个TravelAgency 类，该类两个字段，
    //    用于存储旅游地点和相应的套餐费用。现在，在接受用户输入的信息后，
    //    程序就会要求验证旅行社是否提供该特定的旅游套餐。此处，定义一个Travels 类，
    //    定义一个读/写属性以验证用户输入并对其进行检索。例如，如果旅行社只提供到“加利福尼亚”和
    //    “北京”的旅游服务。
    class Program
    {
        static void Main(string[] args)
        {
            TravelAgency p1 = new TravelAgency("加利福尼亚",3444M);
            TravelAgency p2 = new TravelAgency("北京",5555M);

            Travels pr= new Travels(2);
            pr[0] = p1;
            pr[1] = p2;

            Console.WriteLine("请输入旅游地：");
            string place = Console.ReadLine();

            int j = 0;   
            for (int i = 0; i < 2; i++)
            {
                if (place == pr[i].place)
                {
                    Console.WriteLine("地点：{0}, 费用{1}", pr[place].place, pr[place].cost);
                    j++;
                    break;
                }
            }
            if (j == 0) Console.WriteLine("没有该地");
        }
    }
}
