﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class TravelAgency
    {
        string _tplace;
        decimal _cost;

        public TravelAgency(string place,decimal cost)
        {
            this._tplace = place;
            this._cost = cost;
            }
        public string place
        {
            get {
                return place; 
             }
        }   
        public decimal cost
        {
            get { return cost; }
        }
         
    }
}
