﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Travels
    {
        TravelAgency[] travels;
        public Travels(int length)
        {
            travels = new TravelAgency[length];
        }

        public TravelAgency[] Tr
        {
            get { return travels; }
        }

        public TravelAgency this[int index]
        {
            get
            {
                if (index < 0 || index > travels.Length - 1)
                {
                    return null;
                }
                return travels[index];

            }
            set
            {
                if (index < 0 || index > travels.Length - 1)
                {
                    Console.WriteLine("溢出");
                    return;
                }

                this.travels[index] = value;
            }
        }

        public TravelAgency this[string place]
        {
            get
            {
                if (isE(place))
                {
                    foreach (var t in travels)
                    {
                        if (t.place == place)
                        {
                            return t;
                        }
                    }
                    return null; 
                }
                else
                {
                    return null;
                }




            }
        }
        public bool isE(String tp)
        {

            for (int i = 0; i < travels.Length; i++)
            {
                if (travels[i].place == tp)
                {
                    return true;
                }
            }
            return false;

        }
    }
}
