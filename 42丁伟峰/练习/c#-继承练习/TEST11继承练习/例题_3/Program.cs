﻿using System;

namespace 例题_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Car c = new Car();
            c.Wheels = 4;
            c.Weight = 2.0;
            c.Loader = 5;
            c.Intro();
        }
    }
}
