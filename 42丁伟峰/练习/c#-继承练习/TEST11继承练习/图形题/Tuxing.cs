﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 图形题
{
    class Tuxing
    {
        protected string name;
        public string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        protected double zhoucang;
        protected double mianji;

        public void guieqw()
        {
            Console.WriteLine("名字:{0}\n周长:{1}\n面积:{2}",name,zhoucang,mianji);
        }
    }

    class Shang :Tuxing
    {
        public double a;
        public double b;
        public double c;

        public void jishuang() 
        {
            zhoucang = a + b + c;
            double p = zhoucang / 2;
            mianji = (p * (p - a) * (p - b) * (p - c));//海伦公式
        }
    }

    class Chang : Tuxing 
    {
        public double a;
        public double b;

        public void jishuang2()
        {
            zhoucang = (a + b) * 2;
            mianji = a * b;
        }
    }

    class Yuan : Tuxing 
    {
        public double r;
        public void jishuang3() 
        {
            double d = 3.14;
            zhoucang = 2 * d * r;
            mianji = 2 * d * r * r;
        }
    }
}
