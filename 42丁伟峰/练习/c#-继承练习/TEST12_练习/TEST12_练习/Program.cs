﻿using System;

namespace TEST12_练习
{
    class Program
    {
        static void Main(string[] args)
        {
            Gao g = new Gao("高级主管","北京",1988,5000);
            g.Intro();

            Gao a = new Gao("程序员", "深圳", 1996, 2000);
            a.Intro();

            Pu p = new Pu("秘书","上海",1996,3000);
            p.puIntro();

            Pu u = new Pu("清洁员", "海南", 1973,1000);
            u.puIntro();
        }
    }
}
