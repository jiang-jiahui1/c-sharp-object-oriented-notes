﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TEST12_练习
{
    class YUANg
    {
        protected string name;
        protected string dizhi;
        protected int shenri;
        public YUANg(string name,string dizhi,int shenri)
        {
            this.name = name;
            this.dizhi = dizhi;
            this.shenri = shenri;
        }
    }
    class Gao : YUANg 
    {
        double gongzi;
        public Gao(string name,string dizhi,int shenri,double gongzi):base(name,dizhi,shenri)
        {
            this.gongzi = gongzi;
        }
        public void Intro() 
        {
            Console.WriteLine("姓名：{0}\n地址：{1}\n生日：{2}\n工资：{3}",name,dizhi,shenri,gongzi);
        }
    }
    class Pu : YUANg 
    {
        double pugongzi;
        public Pu(string name, string dizhi, int shenri, double pugong) : base(name, dizhi, shenri)
        {
            this.pugongzi = pugong;
        }
        public void puIntro()
        {
            Console.WriteLine("姓名：{0}\n地址：{1}\n生日：{2}\n工资：{3}", name, dizhi, shenri, pugongzi);
        }
    }
}
