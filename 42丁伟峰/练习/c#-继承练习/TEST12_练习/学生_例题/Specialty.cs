﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 学生_例题
{
    class Specialty
    {
        protected string name;
        protected int age;
        protected string degree;
        public Specialty(string name,int age,string degree) 
        {
            this.name = name;
            this.age = age;
            this.degree = degree;
        }

    }
    class Zhuang : Specialty 
    {
        string spec;
        public Zhuang(string name,int age,string degree,string spec):base(name,age,degree) 
        {
            this.spec = spec;
        }
        public void Intro() 
        {
            Console.WriteLine("姓名:{0}\n 年龄：{1}\n 学位:{2}\n  专业：{3} ", name, age, degree, spec); ;
        }
    }
}
