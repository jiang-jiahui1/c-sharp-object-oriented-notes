﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 学生_例题
{
    class Undergraduate
    {
        protected string name;
        protected int age;
        protected string degree;
        public Undergraduate(string name, int age, string degree)
        {
            this.name = name;
            this.age = age;
            this.degree = degree;
        }

    }
    class Beng : Specialty
    {
        string drec;
        public Beng(string name, int age, string degree, string drec) : base(name, age, degree)
        {
            this.drec = drec;
        }
        public void Intro()
        {
            Console.WriteLine("姓名:{0}\n 年龄：{1}\n 学位:{2}\n  专业：{3} ", name, age, degree, drec) ;
        }
    }
}

