﻿using System;
using System.Text.RegularExpressions;

namespace 正则表达式2
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.	用户输入手机号码，请验证其合法性。
            Console.WriteLine("请输入手机号码");
            string a = Console.ReadLine();

            bool b = Regex.IsMatch(a, @"(\+86)?1\d{10}");

            Console.WriteLine("是否符合"+b);
        }
    }
}
