```C#
using System;

namespace Toubns
{
    class Program
    {
        /*1，雇员系统，定义雇员基类，共同的属性，姓名、地址和出生日期(可有可无)，子类：
        程序员，秘书，高层管理，清洁工，他们有不同的工资算法，其中高级主管和程序员采
        用底薪加提成的方式，高级主管和程序员的底薪分别是5000元和2000元 ，秘书和清洁工
        采用工资的方式，工资分别是3000和1000， */
        static void Main(string[] args)
        {
            Office o = new Office("高级主管","马一","新罗区","19950-08-13");
            o.of();
            Staff s = new Staff("秘书", "胡飞飞", "新罗区", "1999-03-26");
            s.st();
        }
    }
    class Employe
    {
        protected string name;
        protected string site;
        protected string datetime;
        public Employe(string name,string site,string datetime)
        {
            this.name = name;
            this.site = site;
            this.datetime = datetime;
        }
    }
    class Office : Employe
    {
        string worker;
        public Office(string worker,string name, string site, string datetime):base(name,site,datetime)
        {
            this.worker = worker;
            this.name = name;
            this.site = site;
            this.datetime = datetime;
        }
        public void of()
        {
            decimal pay;
            decimal salary;
            decimal wage;
            if (worker == "高级主管")
            {
                pay = 5000;
                salary = 3000;
                wage = pay + salary;
                Console.WriteLine("职位：【{0}】\n姓名：{1}\t地址：{2}\t出生日期：{3}\n工资：{4}",
                    worker,name,site,datetime,wage);
            }
            else if (worker == "程序员")
            {
                pay = 2000;
                salary = 4000;
                wage = pay + salary;
                Console.WriteLine("职位：【{0}】\n姓名：{1}\t地址：{2}\t出生日期：{3}\n工资：{4}",
                    worker, name, site,datetime, wage);
            }
            else { Console.WriteLine("抱歉，没有该职位！！！"); }
        }
    }
    class Staff : Employe 
    {
        string worker;
        public Staff(string worker, string name, string site, string datetime) : base(name, site, datetime)
        {
            this.worker = worker;
            this.name = name;
            this.site = site;
            this.datetime = datetime;
        }
        public void st()
        {
            decimal wage;
            if (worker == "秘书")
            {
                wage = 3000;
                Console.WriteLine("职位：【{0}】\n姓名：{1}\t地址：{2}\t出生日期：{3}\n工资：{4}",
                    worker, name, site,  datetime, wage);
            }
            else if (worker == "程序员")
            {
                wage = 1000;
                Console.WriteLine("职位：【{0}】\n姓名：{1}\t地址：{2}\t出生日期：{3}\n工资：{4}",
                    worker, name, site,  datetime, wage);
            }
            else { Console.WriteLine("抱歉，没有该职位！！！"); }
        }
    }
}
```