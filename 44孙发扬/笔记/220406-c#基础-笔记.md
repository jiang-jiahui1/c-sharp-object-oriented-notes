类与对象深入
    
    访问修饰符
        public: 公开的，无限制条件，任何代码都可以访问；
        internal: 可被同一个程序集的所写代码访问；
        protected: 可被自己或者子类的代码访问；
        private: 私有的，只有自己的代码才能访问；
            一个访问权限最高，任何代码都可以访问--public；
            一个访问权限最低，只有本对象的代码才能访问--private；
        
    面向对象语言的特点：
        封装：将数据或函数（行为）等集合在一个单元中（我们称之为类）；
        继承：类似于现实世界继承的意思；
        多态：一个事物（类）有多种表现形式；

数据类型的分类：

    值类型：
        表示实际数据；直接存储值；数据存储在堆栈中；Int、char等，结构struct
        
        1.简单类型：
            有符号整型：int、long、short、sbyte；
            无符号类型：uint、ulong、ushort、usbyte；
            浮点类型：float、double、decimal
            字符类型：char
            布尔类型：bool

        2.枚举型：enum
        3.结构体类型：struct

    引用类型：
        数据存储在堆中；存储数据的地址；类、数组、字符串

        1.类类型：字符串类型，类类型
        2.数组类型：int[]、string[]
        3.接口类型：interface
        4.委托类型：delegate   
    
    值类型与引用类型区别
        1.值类型在栈中开辟，自动释放，效率更高；引用类型在堆里面开辟，手动释放(GC垃圾回收)，
        2.值类型不可以派生，没多态 ;引用类型可以派生，有多态
            管理机制、内存分配、赋值方式、基类
栈内存与堆内存：

    1)   栈区：由编译器自动分配释放 ，存放值类型的对象本身，引用类型的引用地址（指针），静态区对象的引用地址（指针），
    常量区对象的引用地址（指针）等。其操作方式类似于数据结构中的栈。
    2)   堆区（托管堆）:用于存放引用类型对象本身。在c#中由.net平台的垃圾回收机制（GC）管理。栈，堆都属于动态存储区，
    可以实现动态分配。
    3)   静态区及常量区：用于存放静态类，静态成员（静态变量，静态方法），常量的对象本身。由于存在栈内的引用地址都在程
    序运行开始最先入栈，因此静态区和常量区内的对象的生命周期会持续到程序运行结束时，届时静态区内和常量区内对象才会被释
    放和回收（编译器自动释放）。所以应限制使用静态类，静态成员（静态变量，静态方法），常量，否则程序负荷高。

值类型与引用类型的转换：装箱和拆箱
    
    装箱：将值类型装换为引用类型；隐式操作
    拆箱：将引用类型转换为值类型；显示操作

ref关键字和out关键字：

    ref(引用传递)和out(输出传递)
    参数标记为ref，那么必须在调用函数之前初始化参数的值；
    参数标记为out，调用函数之前不需要初始化对象，但调用的函数必要在函数返回之前为对象赋值

成员方法的重载：

     方法名相同但是
        1.形参个数不同 
        2.类型不一致  
        3.顺序不一致  
            是一个类内部有多个同名的方法，根据方法参数的类型或者个数进行区别。