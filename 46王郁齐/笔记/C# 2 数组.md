# C#第二课

## 枚举

//定义一个枚举类型
        enum week
        {
            Sun,
            Mon,
            Tue,
            Wed,
            Thu,
            Fri,
            Sat
        };

##### 输出

static void Main(string[] args)
        {
            foreach(string i in Enum.GetNames(typeof(week)))
            {
                Console.WriteLine(i);
            }

#### 类型转换

​            ////string str = Console.ReadLine();
​            ////int a  = int.Parse(str);

## 数组

​            数组初始化: **静态初始化**(知道数组的元素-->长度确定，元素确定)

​								 **动态初始化**

​			1.**静态初始化**, var
​            ////var arr = new int[] { 1, 2, 3, 4, 5 };

​			2.**动态初始化**，
​            ////默认值：整型：0   string: null
​            ////数组的下标：从0开始
​            ////int[] arr1 = new int[5]; //0 1 2 3 4
​            ////Console.WriteLine(arr1[0]);

### 随机数

//Random rd = new Random();

### 数组的遍历:

动态赋值， 遍历每一个元素
            ////for(int i = 0; i<5;i++)
            ////{
            ////    arr1[i] = rd.Next(5,10);
            ////}

输出
            ////for (int i = 0; i < 5; i++)
            ////{
            ////    Console.Write(arr1[i]+" ");