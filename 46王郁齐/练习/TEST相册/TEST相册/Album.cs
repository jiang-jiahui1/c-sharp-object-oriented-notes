﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TEST相册
{
    class Album
    {
        private Photo[] photos;
        public Album(int capacity)
        {
            this.photos = new Photo[capacity];
        }

        public Photo this[int index]
        {
            get
            {
                //验证索引器范围
                if (index < 0 || index >= this.photos.Length)
                {
                    Console.WriteLine("索引无效");
                    return null;//失败
                }
                return photos[index];//返回请求的照片
            }

            set
            {
                //验证索引器范围
                if (index < 0 || index >= this.photos.Length)
                {
                    Console.WriteLine("索引无效");
                    return;//失败
                }
                this.photos[index] = value;//向数组加载新的照片
            }
        }
        public Photo this[string title]
        {
            get
            {
                //遍历数组所有的照片
                foreach(Photo p in photos)
                {
                    if (p.Title == title)
                    {
                        return p;
                    }
                }
                Console.WriteLine("未找到");
                //使用null指示失败
                return null;
            }
        }
    }
}
