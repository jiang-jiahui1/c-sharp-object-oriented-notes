﻿using System;

namespace TEST相册
{
    class Program
    {
        static void Main(string[] args)
        {
            //创建容量为3的相册
            Album friends = new Album(3);

            //创建3张照片
            Photo first = new Photo("Jenn");//实例化对象
            Photo second = new Photo("Smith");
            Photo third = new Photo("Mark");

            //向相册加载照片
            friends[0] = first;
            friends[1] = second;
            friends[2] = third;

            //按索引进行检索
            Photo obj1 = friends[2];
            Console.WriteLine(obj1.Title);

            //按名称进行检索
            Photo obj2 = friends["Jenn"];
            Console.WriteLine(obj2.Title);

        }
    }
}
