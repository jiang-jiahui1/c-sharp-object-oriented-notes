﻿using System;

namespace 其他类1
{
    //1.	生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
    internal class Program
    {
        static void Main(string[] args)
        {
            //定义数组
            int[] num = new int[10];
            int[] newnum = new int[10];
            int n;
            int r = 9;
            int temp;
            //创建随机数
            Random random = new Random();

            for (int i = 0; i < 10; i++)
            {
                num[i] = i;
            }

            for (int i = 0; i < 10; i++)
            {
                n = random.Next(0, r);//随机数1到10
                newnum[i] = num[n];//将随机的数当成下标赋值给新的数组下标

                temp = num[n]; //交换避免重复，num[n]和最后一个数交换num[r]
                num[n] = num[r];
                num[r] = temp;
                r--;
            }
            //输出
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine(newnum[i]);
            }

        }
    }
}
