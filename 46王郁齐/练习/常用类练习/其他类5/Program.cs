﻿using System;

namespace 其他类5
{
    class Program
    {
        static void Main(string[] args)
        {
            //5.	本周天和本周六的日期；
            DateTime now = DateTime.Now;
            DayOfWeek a = now.DayOfWeek;
            int b = Convert.ToInt16(a);
            DateTime c = DateTime.Now.AddDays(-b);
            DateTime d = DateTime.Now.AddDays(6 - b);
            Console.WriteLine(c.ToLongDateString());
            Console.WriteLine(d.ToLongTimeString());            
        }
    }
}
