﻿using System;

namespace 索引器旅游例题
{
    //为旅行社编写一个程序，用于接受用户输入的旅游地点。
    //接下来验证旅行社是否能满足用户对地点的请求，验证之后显示相应消息，
    //给出该旅游套餐的费用。注意：定义一个TravelAgency 类，该类两个字段，
    //    用于存储旅游地点和相应的套餐费用。现在，在接受用户输入的信息后，
    //    程序就会要求验证旅行社是否提供该特定的旅游套餐。此处，定义一个Travels 类，
    //    定义一个读/写属性以验证用户输入并对其进行检索。例如，如果旅行社只提供到“加利福尼亚”和
    //    “北京”的旅游服务。
    class Program
    {
        static void Main(string[] args)
        {
            TravelAgency t1 = new TravelAgency("北京",9999M);
            TravelAgency t2 = new TravelAgency("上海",7999M);
            TravelAgency t3 = new TravelAgency("深圳",6999M);
            TravelAgency t4 = new TravelAgency("南京",4999M);

            //实例化Travels
            Travels tr = new Travels(3);

            //将旅行装进travels
            tr[0] = t1;
            tr[1] = t2;
            tr[2] = t3;
            Console.WriteLine("请输入旅游地");
            string place = Console.ReadLine();

            int j = 0;   //标志位
            for (int i = 0; i < 3; i++)
            {
                if (place == tr[i].Tplace)
                {
                    Console.WriteLine("地点：{0}, 费用{1}", tr[place].Tplace, tr[place].Cost);
                    j++;  //标志位+1
                    break;
                }
            }
            if (j == 0) Console.WriteLine("没有该地");
        }
    }
}
