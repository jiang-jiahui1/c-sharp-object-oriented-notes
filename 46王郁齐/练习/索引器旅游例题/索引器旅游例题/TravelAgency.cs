﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 索引器旅游例题
{
    class TravelAgency
    {
        string _tplace;
        decimal _cost;

        public TravelAgency(string place, decimal cost)
        {
            this._tplace = place;
            this._cost = cost;
        }


        //属性方法
        //只读属性
        public string Tplace
        {
            get { return _tplace; }
        }
        //只写属性
        public decimal Cost
        {
            get { return _cost; }
        }

    }
}
