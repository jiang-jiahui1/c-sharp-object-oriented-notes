﻿using System;

namespace Travel
{
    class Program
    {
        static void Main(string[] args)
        {
            TravelAgency t1 = new TravelAgency("北京", 8999M);
            TravelAgency t2 = new TravelAgency("加利福尼亚", 99999999M);
            TravelAgency t3 = new TravelAgency("上海", 2999M);

            //实例化Travels
            Travel tr = new Travel(3);


            //将旅游地装进travels
            tr[0] = t1;   //set
            tr[1] = t2;   //set
            tr[2] = t3;
            Console.WriteLine("请输入旅游地：");
            string place = Console.ReadLine();


            int j = 0;   //标志位
            for (int i = 0; i < 3; i++)
            {
                if (place == tr[i].Tplace)
                {
                    Console.WriteLine("地点：{0}, 费用{1}", tr[place].Tplace, tr[place].Cost);
                    j++;  //标志位+1
                    break;
                }
            }
            if (j == 0) Console.WriteLine("没有该地");

        }
    }

}
