﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel
{
    class Travel
    {
        //三、编程题
        //1、为旅行社编写一个程序，用于接受用户输入的旅游地点。
        //接下来验证旅行社是否能满足用户对地点的请求，验证
        //之后显示相应消息，给出该旅游套餐的费用。注意：定
        //义一个TravelAgency 类，该类两个字段，用于存储旅游
        //地点和相应的套餐费用。现在，在接受用户输入的信息
        //后，程序就会要求验证旅行社是否提供该特定的旅游套餐。
        //此处，定义一个Travels 类，定义一个读/写属性以验证
        //用户输入并对其进行检索。例如，如果旅行社只提供到
        //“加利福尼亚”和“北京”的旅游服务。

        TravelAgency[] travel; //声明"travel"属性

        public Travel(int length) //初始化属性值~
        {
            travel = new TravelAgency[length];
        }

        public TravelAgency[] Tr
        {
            get { return travel; }
        }

        public TravelAgency this[int index]
        {
            get 
            {
                if (index < 0 || index >= travel.Length)
                {
                    return null;
                }
                return travel[index];
            }

            set
            {
                if (index < 0 || index >= travel.Length)
                {
                    Console.WriteLine("溢出");
                    return;
                }
                travel[index] = value;

            }

        }


        public bool isE(String tp)
        {

            for (int i = 0; i < travel.Length; i++)
            {
                if (travel[i].Tplace == tp)
                {
                    return true;
                }
            }
            return false;

        }



        public TravelAgency this[string place]
        {
            get
            {
                if (isE(place))
                {
                    foreach (var t in travel)
                    {
                        if (t.Tplace == place)
                        {
                            return t;
                        }
                    }
                    return null;
                }
                else
                {
                    return null;
                }

            }
        }




    }
}
