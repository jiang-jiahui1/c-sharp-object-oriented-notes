﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travel
{
    class TravelAgency
    {
        string _tplace;
        decimal _cost;

        public TravelAgency(string place, decimal cost)
        {
            this._tplace = place;
            this._cost = cost;
        }




        public string Tplace
        {
            get { return _tplace; }
        }

        public decimal Cost
        {
            get { return _cost; }
        }

    }
}
