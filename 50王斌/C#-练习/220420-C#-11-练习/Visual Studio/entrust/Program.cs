﻿using System;

namespace entrust
{
    delegate int myDelegate(int[] arr);
    class Program
    {
        static void Main(string[] args)
        {
            //例题1：Program定义静态方法:  getValue()，用于获取数组arr{1,3,5}的最大值，定义一个委托，并将getValue方法传给委托实例md.

            static void Main(string[] args)
            {
                int[] arr = { 1, 3, 5 };
              
                myDelegate md = getValue;
                int max = md(arr);
                Console.WriteLine(max);

            }

            static int getValue(int[] arr)
            {
                int max = arr[0];
                foreach (var i in arr)
                {
                    if (max < i)
                    {
                        max = i;
                    }
                }

                return max;
            }
        }
    }

    class bendan
    {
        delegate void myDelegate(int[] n);



        //例题2：匿名方法：Square(int n)用于返回n的平方值。 再定义数组arr{1,3,5},定义一个Util类，成员方 法：transForm用于求数组
        //                 内的每个元素的平方值

        static void Main(string[] args)
        {
            int[] n = { 1, 3, 5 };

            myDelegate md = delegate (int[] n)
            {
                for (int i = 0; i < n.Length; i++)
                {
                    n[i] = n[i] * n[i];
                }
            };

            md(n);

            foreach (var i in n)
            {
                Console.WriteLine(i);
            }
        }
    }
}
