一、

//雇员系统，定义雇员基类，共同的属性，姓名、地址和出生日期(可有可无)，
//子类：程序员，秘书，高层管理，清洁工，
//他们有不同的工资算法，其中高级主管和程序员采用底薪加提成(6000)的方式，
//高级主管和程序员的底薪分别是5000元和2000元?，秘书和清洁工采用工资的方式，
//工资分别是3000和1000，（以多态的方式处理程序。）






二、

创建一个Shape类,此类包含一个名为color的数据成员(用于存放颜色值)和一个
GetColor方法(用于获取颜色值)，这个类还包含一个名为GetArea的虚方法。
用这个类创建名为Circle和Square的两个子类，这两个类都包含两个数据成员，
即radius和sideLen。这些派生类应提供GetArea方法的实现，以计算相应形状的
面积。




三、
将加减乘除四则运算看作一种操作,请设计操作抽象类Operation，
该抽象类有一个Op的方法计算两个整数相应运算，并返回计算结果。
并派生出四则运算操作类。计算器一个方法Do方法


