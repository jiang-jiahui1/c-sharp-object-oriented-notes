﻿using System;

namespace 其他类3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //3.	生成4-7之间的随机小数，保留两位小数。
            Random random = new Random();
            int a = random.Next(4,7);
            double b = random.NextDouble();
            Console.WriteLine(string.Format("{0:F2}", (a + b)));

        }
    }
}
