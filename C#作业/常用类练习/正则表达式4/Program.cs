﻿using System;
using System.Text.RegularExpressions;
namespace 正则表达式4
{
    class Program
    {
        static void Main(string[] args)
        {
            //4.	使用Regex实现“123abc456ABC789”中每一个数字替换成“X”。
            string F = "123abc456ABC789";
            Regex c = new Regex(@"\d");
            string a = c.Replace(F,"X");
            Console.WriteLine(a);
        }
    }
}
